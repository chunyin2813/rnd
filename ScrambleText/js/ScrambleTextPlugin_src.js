/*!
 * VERSION: 0.5.2
 * DATE: 2018-09-11
 * UPDATES AND DOCS AT: http://greensock.com
 *
 * @license Copyright (c) 2008-2017, GreenSock. All rights reserved.
 * ScrambleTextPlugin is a Club GreenSock membership benefit; You must have a valid membership to use
 * this code without violating the terms of use. Visit http://greensock.com/club/ to sign up or get more details.
 * This work is subject to the software agreement that was issued with your membership.
 *
 * @author: Jack Doyle, jack@greensock.com
 */
var _gsScope = "undefined" != typeof module && module.exports && "undefined" != typeof global ? global : this || window;
(_gsScope._gsQueue || (_gsScope._gsQueue = [])).push(function () {
        "use strict";
        var s = /(^\s+|\s+$)/g,
            _ = /\s+/g,
            g = function (t) {
                var e = t.nodeType,
                    i = "";
                if (1 === e || 9 === e || 11 === e) {
                    if ("string" == typeof t.textContent) return t.textContent;
                    for (t = t.firstChild; t; t = t.nextSibling) i += g(t)
                } else if (3 === e || 4 === e) return t.nodeValue;
                return i
            },
            i = function (t, e) {
                for (var i = e.length, s = ""; - 1 < --t;) s += e[Math.random() * i | 0];
                return s
            },
            d = function (t) {
                var e;
                for (this.chars = C(t), this.sets = [], this.length = 50, e = 0; e < 20; e++) this.sets[e] = i(80, this.chars);
                this.grow = function (t) {
                    for (e = 0; e < 20; e++) this.sets[e] += i(t - this.length, this.chars);
                    this.length = t
                }
            },
            t = "[顎€-铮縘|\ud83c[\udc00-\udfff]|\ud83d[\udc00-\udfff]|[鈿�-鈿梋|\ud83e[\udd10-\udd5d]|[\ud800-\udbff][\udc00-\udfff]",
            f = new RegExp(t),
            n = new RegExp(t + "|.", "g"),
            C = function (t, e, i) {
                return i && (t = t.replace(s, "")), "" !== e && e || !f.test(t) ? t.split(e || "") : t.match(n)
            },
            e = "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            r = e.toLowerCase(),
            u = {
                upperCase: new d(e),
                lowerCase: new d(r),
                upperAndLowerCase: new d(e + r)
            },
            c = "ScrambleTextPlugin",
            p = String.fromCharCode(103, 114, 101, 101, 110, 115, 111, 99, 107, 46, 99, 111, 109),
            m = String.fromCharCode(47, 114, 101, 113, 117, 105, 114, 101, 115, 45, 109, 101, 109, 98, 101, 114, 115, 104, 105, 112, 47),
            w = function (t) {
                for (var e = -1 !== (window ? 'codepen.io' : "").indexOf(String.fromCharCode(103, 114, 101, 101, 110, 115, 111, 99, 107)) && -1 !== t.indexOf(String.fromCharCode(108, 111, 99, 97, 108, 104, 111, 115, 116)), i = [p, String.fromCharCode(99, 111, 100, 101, 112, 101, 110, 46, 105, 111), String.fromCharCode(99, 111, 100, 101, 112, 101, 110, 46, 112, 108, 117, 109, 98, 105, 110, 103), String.fromCharCode(99, 111, 100, 101, 112, 101, 110, 46, 100, 101, 118), String.fromCharCode(99, 115, 115, 45, 116, 114, 105, 99, 107, 115, 46, 99, 111, 109), String.fromCharCode(99, 100, 112, 110, 46, 105, 111), String.fromCharCode(103, 97, 110, 110, 111, 110, 46, 116, 118), String.fromCharCode(99, 111, 100, 101, 99, 97, 110, 121, 111, 110, 46, 110, 101, 116), String.fromCharCode(116, 104, 101, 109, 101, 102, 111, 114, 101, 115, 116, 46, 110, 101, 116), String.fromCharCode(99, 101, 114, 101, 98, 114, 97, 120, 46, 99, 111, 46, 117, 107), String.fromCharCode(116, 121, 109, 112, 97, 110, 117, 115, 46, 110, 101, 116), String.fromCharCode(116, 119, 101, 101, 110, 109, 97, 120, 46, 99, 111, 109), String.fromCharCode(116, 119, 101, 101, 110, 108, 105, 116, 101, 46, 99, 111, 109), String.fromCharCode(112, 108, 110, 107, 114, 46, 99, 111), String.fromCharCode(104, 111, 116, 106, 97, 114, 46, 99, 111, 109), String.fromCharCode(119, 101, 98, 112, 97, 99, 107, 98, 105, 110, 46, 99, 111, 109), String.fromCharCode(97, 114, 99, 104, 105, 118, 101, 46, 111, 114, 103), String.fromCharCode(99, 111, 100, 101, 115, 97, 110, 100, 98, 111, 120, 46, 105, 111), String.fromCharCode(115, 116, 97, 99, 107, 98, 108, 105, 116, 122, 46, 99, 111, 109), String.fromCharCode(99, 111, 100, 105, 101, 114, 46, 105, 111), String.fromCharCode(106, 115, 102, 105, 100, 100, 108, 101, 46, 110, 101, 116)], s = i.length; - 1 < --s;)
                    if (-1 !== t.indexOf(i[s])) return !0;
                return e && window && window.console && console.log(String.fromCharCode(87, 65, 82, 78, 73, 78, 71, 58, 32, 97, 32, 115, 112, 101, 99, 105, 97, 108, 32, 118, 101, 114, 115, 105, 111, 110, 32, 111, 102, 32) + c + String.fromCharCode(32, 105, 115, 32, 114, 117, 110, 110, 105, 110, 103, 32, 108, 111, 99, 97, 108, 108, 121, 44, 32, 98, 117, 116, 32, 105, 116, 32, 119, 105, 108, 108, 32, 110, 111, 116, 32, 119, 111, 114, 107, 32, 111, 110, 32, 97, 32, 108, 105, 118, 101, 32, 100, 111, 109, 97, 105, 110, 32, 98, 101, 99, 97, 117, 115, 101, 32, 105, 116, 32, 105, 115, 32, 97, 32, 109, 101, 109, 98, 101, 114, 115, 104, 105, 112, 32, 98, 101, 110, 101, 102, 105, 116, 32, 111, 102, 32, 67, 108, 117, 98, 32, 71, 114, 101, 101, 110, 83, 111, 99, 107, 46, 32, 80, 108, 101, 97, 115, 101, 32, 115, 105, 103, 110, 32, 117, 112, 32, 97, 116, 32, 104, 116, 116, 112, 58, 47, 47, 103, 114, 101, 101, 110, 115, 111, 99, 107, 46, 99, 111, 109, 47, 99, 108, 117, 98, 47, 32, 97, 110, 100, 32, 116, 104, 101, 110, 32, 100, 111, 119, 110, 108, 111, 97, 100, 32, 116, 104, 101, 32, 39, 114, 101, 97, 108, 39, 32, 118, 101, 114, 115, 105, 111, 110, 32, 102, 114, 111, 109, 32, 121, 111, 117, 114, 32, 71, 114, 101, 101, 110, 83, 111, 99, 107, 32, 97, 99, 99, 111, 117, 110, 116, 32, 119, 104, 105, 99, 104, 32, 104, 97, 115, 32, 110, 111, 32, 115, 117, 99, 104, 32, 108, 105, 109, 105, 116, 97, 116, 105, 111, 110, 115, 46, 32, 84, 104, 101, 32, 102, 105, 108, 101, 32, 121, 111, 117, 39, 114, 101, 32, 117, 115, 105, 110, 103, 32, 119, 97, 115, 32, 108, 105, 107, 101, 108, 121, 32, 100, 111, 119, 110, 108, 111, 97, 100, 101, 100, 32, 102, 114, 111, 109, 32, 101, 108, 115, 101, 119, 104, 101, 114, 101, 32, 111, 110, 32, 116, 104, 101, 32, 119, 101, 98, 32, 97, 110, 100, 32, 105, 115, 32, 114, 101, 115, 116, 114, 105, 99, 116, 101, 100, 32, 116, 111, 32, 108, 111, 99, 97, 108, 32, 117, 115, 101, 32, 111, 114, 32, 111, 110, 32, 115, 105, 116, 101, 115, 32, 108, 105, 107, 101, 32, 99, 111, 100, 101, 112, 101, 110, 46, 105, 111, 46)), e
            }(window ? 'codepen.io' : ""),
            h = _gsScope._gsDefine.plugin({
                propName: "scrambleText",
                version: "0.5.2",
                API: 2,
                overwriteProps: ["scrambleText", "text"],
                init: function (t, e, i, s) {
                    if (this._prop = "innerHTML" in t ? "innerHTML" : "textContent" in t ? "textContent" : 0, !this._prop) return !1;
                    if (!w) return window.location.href = "http://" + p + m + "?plugin=" + c + "&source=codepen", !1;
                    "function" == typeof e && (e = e(s, t)), this._target = t, "object" != typeof e && (e = {
                        text: e
                    });
                    var n, r, h, o, a = e.text || e.value,
                        l = !1 !== e.trim;
                    return this._delimiter = n = e.delimiter || "", this._original = C(g(t).replace(_, " ").split("&nbsp;").join(""), n, l), "{original}" !== a && !0 !== a && null != a || (a = this._original.join(n)), this._text = C((a || "").replace(_, " "), n, l), this._hasClass = !1, "string" == typeof e.newClass && (this._newClass = e.newClass, this._hasClass = !0), "string" == typeof e.oldClass && (this._oldClass = e.oldClass, this._hasClass = !0), o = "" === n, this._textHasEmoji = f.test(this._text.join(n)) && o, this._charsHaveEmoji = !!e.chars && f.test(e.chars), this._length = o ? this._original.length : this._original.join(n).length, this._lengthDif = (o ? this._text.length : this._text.join(n).length) - this._length, this._fillChar = e.fillChar || e.chars && -1 !== e.chars.indexOf(" ") ? "&nbsp;" : "", this._charSet = h = u[e.chars || "upperCase"] || new d(e.chars), this._speed = .016 / (e.speed || 1), this._prevScrambleTime = 0, this._setIndex = 20 * Math.random() | 0, (r = this._length + Math.max(this._lengthDif, 0)) > h.length && h.grow(r), this._chars = h.sets[this._setIndex], this._revealDelay = e.revealDelay || 0, this._tweenLength = !1 !== e.tweenLength, this._tween = i, this._rightToLeft = !!e.rightToLeft, w
                },
                set: function (t) {
                    var e, i, s, n, r, h, o, a, l, _ = this._text.length,
                        g = this._delimiter,
                        d = this._tween._time,
                        f = d - this._prevScrambleTime;
                    this._revealDelay && (this._tween.vars.runBackwards && (d = this._tween._duration - d), t = 0 === d ? 0 : d < this._revealDelay ? 1e-6 : d === this._tween._duration ? 1 : this._tween._ease.getRatio((d - this._revealDelay) / (this._tween._duration - this._revealDelay))), t < 0 ? t = 0 : 1 < t && (t = 1), this._rightToLeft && (t = 1 - t), e = t * _ + .5 | 0, t ? ((f > this._speed || f < -this._speed) && (this._setIndex = (this._setIndex + (19 * Math.random() | 0)) % 20, this._chars = this._charSet.sets[this._setIndex], this._prevScrambleTime += f), n = this._chars) : n = this._original.join(g), this._rightToLeft ? 1 !== t || !this._tween.vars.runBackwards && "isFromStart" !== this._tween.data ? (o = this._text.slice(e).join(g), s = this._charsHaveEmoji ? C(n).slice(0, this._length + (this._tweenLength ? 1 - t * t * t : 1) * this._lengthDif - (this._textHasEmoji ? C(o) : o).length + .5 | 0).join("") : n.substr(0, this._length + (this._tweenLength ? 1 - t * t * t : 1) * this._lengthDif - (this._textHasEmoji ? C(o) : o).length + .5 | 0), n = o) : (s = "", n = this._original.join(g)) : (s = this._text.slice(0, e).join(g), i = (this._textHasEmoji ? C(s) : s).length, n = this._charsHaveEmoji ? C(n).slice(i, this._length + (this._tweenLength ? 1 - (t = 1 - t) * t * t * t : 1) * this._lengthDif + .5 | 0).join("") : n.substr(i, this._length + (this._tweenLength ? 1 - (t = 1 - t) * t * t * t : 1) * this._lengthDif - i + .5 | 0)), o = this._hasClass ? ((r = (a = this._rightToLeft ? this._oldClass : this._newClass) && 0 !== e) ? "<span class='" + a + "'>" : "") + s + (r ? "</span>" : "") + ((h = (l = this._rightToLeft ? this._newClass : this._oldClass) && e !== _) ? "<span class='" + l + "'>" : "") + g + n + (h ? "</span>" : "") : s + g + n, this._target[this._prop] = "&nbsp;" === this._fillChar && -1 !== o.indexOf("  ") ? o.split("  ").join("&nbsp;&nbsp;") : o
                }
            }).prototype;
        for (h in h._newClass = h._oldClass = "", u) u[h.toLowerCase()] = u[h], u[h.toUpperCase()] = u[h]
    }), _gsScope._gsDefine && _gsScope._gsQueue.pop()(),
    function (t) {
        "use strict";
        var e = function () {
            return (_gsScope.GreenSockGlobals || _gsScope).ScrambleTextPlugin
        };
        "undefined" != typeof module && module.exports ? (require("../TweenLite.js"), module.exports = e()) : "function" == typeof define && define.amd && define(["TweenLite"], e)
    }();