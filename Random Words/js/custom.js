/*
See https://www.greensock.com/splittext/ for details. 
This demo uses SplitText which is a membership benefit of Club GreenSock, https://www.greensock.com/club/
*/

var mySplitText = new SplitText("#quote", {
        type: "words"
    }),
    tl = new TimelineLite(),
    numWords = mySplitText.words.length;

//due to bugs in Chrome that caused vibrating while scaling (unrelated to GSAP), we had to delay the start by a brief moment (to let it render at full size once), set will-change:transform, and then animate things in. SUPER annoying. Hopefully Chrome will fix it soon. 
TweenLite.delayedCall(0.08, function () {
    for (var i = 0; i < numWords; i++) {
        tl.from(mySplitText.words[i], 1, {
            force3D: true,
            scale: Math.random() > 0.5 ? 0 : 2,
            opacity: 0
        }, Math.random());
    }
});