(self.webpackChunk_N_E = self.webpackChunk_N_E || []).push([
    [521], {
        9664: function (t, e) {
            ! function (t) {
                "use strict";
                var e, n, i, o, l, r, a, c = function () {
                        return "undefined" !== typeof window
                    },
                    d = function () {
                        return e || c() && (e = window.gsap) && e.registerPlugin && e
                    },
                    u = function (t) {
                        return "string" === typeof t
                    },
                    s = function (t) {
                        return "function" === typeof t
                    },
                    p = function (t, e) {
                        var n = "x" === e ? "Width" : "Height",
                            r = "scroll" + n,
                            a = "client" + n;
                        return t === i || t === o || t === l ? Math.max(o[r], l[r]) - (i["inner" + n] || o[a] || l[a]) : t[r] - t["offset" + n]
                    },
                    f = function (t, e) {
                        var n = "scroll" + ("x" === e ? "Left" : "Top");
                        return t === i && (null != t.pageXOffset ? n = "page" + e.toUpperCase() + "Offset" : t = null != o[n] ? o : l),
                            function () {
                                return t[n]
                            }
                    },
                    m = function (t, e, n, i) {
                        if (s(t) && (t = t(e, n, i)), "object" !== typeof t) return u(t) && "max" !== t && "=" !== t.charAt(1) ? {
                            x: t,
                            y: t
                        } : {
                            y: t
                        };
                        if (t.nodeType) return {
                            y: t,
                            x: t
                        };
                        var o, l = {};
                        for (o in t) l[o] = "onAutoKill" !== o && s(t[o]) ? t[o](e, n, i) : t[o];
                        return l
                    },
                    g = function (t, e) {
                        if (!(t = r(t)[0]) || !t.getBoundingClientRect) return console.warn("scrollTo target doesn't exist. Using 0") || {
                            x: 0,
                            y: 0
                        };
                        var n = t.getBoundingClientRect(),
                            a = !e || e === i || e === l,
                            c = a ? {
                                top: o.clientTop - (i.pageYOffset || o.scrollTop || l.scrollTop || 0),
                                left: o.clientLeft - (i.pageXOffset || o.scrollLeft || l.scrollLeft || 0)
                            } : e.getBoundingClientRect(),
                            d = {
                                x: n.left - c.left,
                                y: n.top - c.top
                            };
                        return !a && e && (d.x += f(e, "x")(), d.y += f(e, "y")()), d
                    },
                    v = function (t, e, n, i, o) {
                        return isNaN(t) || "object" === typeof t ? u(t) && "=" === t.charAt(1) ? parseFloat(t.substr(2)) * ("-" === t.charAt(0) ? -1 : 1) + i - o : "max" === t ? p(e, n) - o : Math.min(p(e, n), g(t, e)[n] - o) : parseFloat(t) - o
                    },
                    h = function () {
                        e = d(), c() && e && document.body && (i = window, l = document.body, o = document.documentElement, r = e.utils.toArray, e.config({
                            autoKillThreshold: 7
                        }), a = e.config(), n = 1)
                    },
                    y = {
                        version: "3.6.1",
                        name: "scrollTo",
                        rawVars: 1,
                        register: function (t) {
                            e = t, h()
                        },
                        init: function (t, e, o, l, r) {
                            n || h();
                            var a = this;
                            a.isWin = t === i, a.target = t, a.tween = o, e = m(e, l, t, r), a.vars = e, a.autoKill = !!e.autoKill, a.getX = f(t, "x"), a.getY = f(t, "y"), a.x = a.xPrev = a.getX(), a.y = a.yPrev = a.getY(), null != e.x ? (a.add(a, "x", a.x, v(e.x, t, "x", a.x, e.offsetX || 0), l, r), a._props.push("scrollTo_x")) : a.skipX = 1, null != e.y ? (a.add(a, "y", a.y, v(e.y, t, "y", a.y, e.offsetY || 0), l, r), a._props.push("scrollTo_y")) : a.skipY = 1
                        },
                        render: function (t, e) {
                            for (var n, o, l, r, c, d = e._pt, u = e.target, s = e.tween, f = e.autoKill, m = e.xPrev, g = e.yPrev, v = e.isWin; d;) d.r(t, d.d), d = d._next;
                            n = v || !e.skipX ? e.getX() : m, l = (o = v || !e.skipY ? e.getY() : g) - g, r = n - m, c = a.autoKillThreshold, e.x < 0 && (e.x = 0), e.y < 0 && (e.y = 0), f && (!e.skipX && (r > c || r < -c) && n < p(u, "x") && (e.skipX = 1), !e.skipY && (l > c || l < -c) && o < p(u, "y") && (e.skipY = 1), e.skipX && e.skipY && (s.kill(), e.vars.onAutoKill && e.vars.onAutoKill.apply(s, e.vars.onAutoKillParams || []))), v ? i.scrollTo(e.skipX ? n : e.x, e.skipY ? o : e.y) : (e.skipY || (u.scrollTop = e.y), e.skipX || (u.scrollLeft = e.x)), e.xPrev = e.x, e.yPrev = e.y
                        },
                        kill: function (t) {
                            var e = "scrollTo" === t;
                            (e || "scrollTo_x" === t) && (this.skipX = 1), (e || "scrollTo_y" === t) && (this.skipY = 1)
                        }
                    };
                y.max = p, y.getOffset = g, y.buildGetter = f, d() && e.registerPlugin(y), t.ScrollToPlugin = y, t.default = y, Object.defineProperty(t, "__esModule", {
                    value: !0
                })
            }(e)
        },
        9911: function (t, e, n) {
            "use strict";
            n.d(e, {
                Z: function () {
                    return C
                }
            });
            var i = n(7294),
                o = n(9163),
                l = n(6358),
                r = n(9664),
                a = n.n(r),
                c = n(7087),
                d = n(4640),
                u = n(2430),
                s = n(1229),
                p = n(4121),
                f = n(8347),
                m = n(131),
                g = i.createElement,
                v = o.ZP.video.withConfig({
                    displayName: "LazyVideo__StyledVideo",
                    componentId: "sc-12k5ev0-0"
                })(["display:block;width:100%;height:100%;object-fit:cover;"]),
                h = function (t) {
                    var e = t.src,
                        n = t.style,
                        o = t.className,
                        l = (0, f.Z)(t, ["src", "style", "className"]),
                        r = (0, i.useRef)(!1),
                        a = (0, m.YD)({
                            rootMargin: "100%",
                            triggerOnce: !0
                        }),
                        c = (0, p.Z)(a, 2),
                        d = c[0],
                        u = c[1],
                        h = (0, m.YD)(),
                        y = (0, p.Z)(h, 3),
                        b = y[0],
                        x = y[1],
                        _ = y[2];
                    return (0, i.useEffect)((function () {
                        var t = null === _ || void 0 === _ ? void 0 : _.target;
                        t && (u && !r.current && (t.load(), r.current = !0), x ? function (t) {
                            if (t) {
                                var e = t.play();
                                void 0 !== e && e.then((function () {})).catch((function () {}))
                            }
                        }(t) : function (t) {
                            var e = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
                            t && (t.pause(), e && (t.currentTime = 0))
                        }(t))
                    }), [x, u, _]), g("div", {
                        style: n,
                        className: o,
                        ref: d
                    }, g(v, (0, s.Z)({
                        playsInline: !0,
                        preload: "none",
                        ref: b
                    }, l), g("source", {
                        src: e
                    })))
                },
                y = i.createElement,
                b = o.ZP.div.withConfig({
                    displayName: "BigTextWithVideo__JumboTextWrap",
                    componentId: "sc-1pyvynd-0"
                })(["position:relative;text-align:center;font-size:0;"]),
                x = (0, o.ZP)(h).withConfig({
                    displayName: "BigTextWithVideo__MaskedVideo",
                    componentId: "sc-1pyvynd-1"
                })(["position:absolute;top:1px;left:1px;height:calc(100% - 2px);width:calc(100% - 2px);.ReactModal__Body--open &{opacity:0;}"]),
                _ = o.ZP.svg.withConfig({
                    displayName: "BigTextWithVideo__VideoMask",
                    componentId: "sc-1pyvynd-2"
                })(["position:relative;font-family:var(--secondary-font);text-transform:uppercase;font-size:", ";line-height:.93;.filler{visibility:hidden;}"], (0, c.hO)(400)),
                w = o.ZP.rect.withConfig({
                    displayName: "BigTextWithVideo__BGRect",
                    componentId: "sc-1pyvynd-3"
                })(["fill:var(--brand-white);"]),
                k = o.ZP.rect.withConfig({
                    displayName: "BigTextWithVideo__BlackBox",
                    componentId: "sc-1pyvynd-4"
                })(["fill:#000;"]),
                Z = (0, o.ZP)(u.Z).withConfig({
                    displayName: "BigTextWithVideo__StyledPlayButton",
                    componentId: "sc-1pyvynd-5"
                })(["position:absolute;top:50%;left:50%;transform:translate(-50%,-50%);"]),
                C = function (t) {
                    var e = t.topText,
                        n = t.bottomText,
                        o = t.video,
                        r = t.videoId,
                        c = (0, d.dd)().toggleModal,
                        u = (0, i.useRef)(),
                        s = (0, i.useRef)(),
                        p = (0, i.useRef)(),
                        f = (0, i.useCallback)((function (t) {
                            if (t) {
                                var e = t.querySelector(".filler"),
                                    n = t.querySelector(".the-text"),
                                    i = t.querySelector(".black-box"),
                                    o = t.querySelector(".play-button");
                                u.current = t, l.ZP.set([e, n, i], {
                                    autoAlpha: 1,
                                    transformOrigin: "50% 50%"
                                }), l.ZP.to(n, {
                                    x: 1
                                }), p.current = l.ZP.timeline({
                                    paused: !0,
                                    onComplete: function () {
                                        s.current.progress(0).pause()
                                    }
                                }).to(i, {
                                    opacity: 1,
                                    duration: 2
                                }).fromTo(i, {
                                    opacity: 1
                                }, {
                                    opacity: 0,
                                    duration: 1
                                }), s.current = l.ZP.timeline({
                                    paused: !0,
                                    onComplete: function () {
                                        c(r), p.current.play(0)
                                    }
                                }).fromTo(n, {
                                    scaleY: 1,
                                    scaleX: 1
                                }, {
                                    scaleY: .333,
                                    scaleX: 1.333,
                                    duration: .333
                                }, 0).fromTo(o, {
                                    opacity: 1,
                                    scale: 1
                                }, {
                                    opacity: 0,
                                    scale: .5
                                }, 0).fromTo([e, i], {
                                    scaleY: 0,
                                    scaleX: .5
                                }, {
                                    ease: "circ.inOut",
                                    scaleY: 1,
                                    scaleX: 1,
                                    duration: .5
                                }, 0).fromTo(i, {
                                    opacity: 0
                                }, {
                                    opacity: 1,
                                    duration: 1
                                }, "-=.333")
                            }
                        }), [c, r]);
                    return (0, i.useEffect)((function () {
                        return function () {
                            s.current && s.current.kill(), p.current && p.current.kill()
                        }
                    }), []), y(b, {
                        ref: f
                    }, y(x, {
                        muted: !0,
                        loop: !0,
                        playsInline: !0,
                        src: null === o || void 0 === o ? void 0 : o.url
                    }), y(_, {
                        viewBox: "0 0 1280 720"
                    }, y("g", {
                        fillRule: "evenodd"
                    }, y("mask", {
                        id: "text-mask-".concat(r)
                    }, y("rect", {
                        fill: "#fff",
                        x: "0",
                        y: "0",
                        width: "1280",
                        height: "720"
                    }), y("rect", {
                        className: "filler",
                        fill: "#000",
                        x: "0",
                        y: "0",
                        width: "1280",
                        height: "720"
                    }), y("g", {
                        className: "the-text"
                    }, y("text", {
                        fill: "#000",
                        x: "640",
                        y: "345",
                        textAnchor: "middle"
                    }, e), y("text", {
                        fill: "#000",
                        x: "640",
                        y: "335",
                        textAnchor: "middle",
                        dominantBaseline: "hanging"
                    }, n)))), y(w, {
                        mask: "url(#text-mask-".concat(r, ")"),
                        x: "0",
                        y: "0",
                        width: "1280",
                        height: "720"
                    }), y(k, {
                        className: "black-box",
                        x: "0",
                        y: "0",
                        width: "1280",
                        height: "720",
                        opacity: "0"
                    })), r ? y(Z, {
                        className: "play-button",
                        onClick: function () {
                            s.current.play(0), l.ZP.registerPlugin(a()), l.ZP.to(window, {
                                duration: .4,
                                scrollTo: {
                                    y: u.current,
                                    offsetY: (window.innerHeight - u.current.getBoundingClientRect().height) / 2,
                                    autoKill: !1
                                }
                            })
                        },
                        label: "Play video"
                    }) : null)
                }
        },
        6117: function (t, e, n) {
            "use strict";
            n.r(e), n.d(e, {
                __N_SSG: function () {
                    return lt
                },
                default: function () {
                    return rt
                }
            });
            var i = n(7294),
                o = n(4593),
                l = n(9163),
                r = n(2792),
                a = n(2751),
                c = n(8092),
                d = n(7706),
                u = n(9911),
                s = i.createElement,
                p = (l.ZP)(c.Z).withConfig({
                    displayName: "Video__StyledSection",
                    componentId: "bo9b66-0"
                })(["padding:320px 0 var(--spacing);", " ", ""], r.Z.below(a.bp.desktop, "\n    padding-top: 120px;\n  "), r.Z.below(a.bp.laptopSm, "\n    padding-top: 160px;\n  ")),
                f = function (t) {
                    var e = t.topText,
                        n = t.bottomText,
                        i = t.video,
                        o = t.videoId;
                    return s(p, {
                        bgColor: "brand-white"
                    }, s(d.Z, null, s(u.Z, {
                        topText: e,
                        bottomText: n,
                        video: i,
                        videoId: o
                    })))
                },
                I = i.createElement,
                O = function () {
                    return I(c.Z)
                },
                et = n(1407),
                nt = n(4640),
                it = n(8130),
                ot = i.createElement,
                lt = !0,
                rt = function (t) {
                    var l = t.page,
                        r = (0, nt.dd)().modalOpen;
                    if (!l) return null;
                    var a = l.data;
                    return ot(i.Fragment, null, ot(o.q, {
                    }), ot("main", null, ot(f, {
                        topText: null === a || void 0 === a ? void 0 : a.hero_top_text,
                        bottomText: null === a || void 0 === a ? void 0 : a.hero_bottom_text,
                        video: null === a || void 0 === a ? void 0 : a.hero_video,
                        videoId: null === a || void 0 === a ? void 0 : a.hero_video_id
                    }), ot(O, {
                        title: null === a || void 0 === a ? void 0 : a.two_column_title,
                        description: null === a || void 0 === a ? void 0 : a.two_column_description,
                        image: null === a || void 0 === a ? void 0 : a.two_column_image
                    })), ot(et.Z, null, ot(it.Z, {
                        youTubeID: r
                    })))
                };
        },
        2088: function (t, e, n) {
            (window.__NEXT_P = window.__NEXT_P || []).push(["/", function () {
                return n(6117)
            }])
        }
    },
    function (t) {
        t.O(0, [774, 351, 663, 46, 258, 833, 800, 442, 817], (function () {
            return e = 2088, t(t.s = e);
            var e
        }));
        var e = t.O();
        _N_E = e
    }
]);