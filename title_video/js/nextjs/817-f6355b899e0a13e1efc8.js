(self.webpackChunk_N_E = self.webpackChunk_N_E || []).push([
    [817], {
        9778: function (t, n, e) {
            "use strict";
            var o = e(7294).createElement;
            n.Z = function () {
                return o("svg", {
                    viewBox: "0 0 19 36",
                    fill: "none",
                    xmlns: "http://www.w3.org/2000/svg"
                }, o("path", {
                    d: "M0 20.102C5.979 13.784 18.15.919 19 0l-6.378 15.938H19L1.329 35.753l5.846-17.23L0 20.101z",
                    fill: "currentColor"
                }))
            }
        },
        9520: function (t, n, e) {
            "use strict";
            var o = e(1229),
                r = e(8347),
                i = e(7294),
                a = e(9163),
                c = e(7087),
                l = e(2751),
                s = e(2334),
                u = e(8362),
                p = i.createElement,
                d = (0, a.ZP)(u.Z).withConfig({
                    displayName: "Button__StyledButton",
                    componentId: "sc-1webfnb-0"
                })(["display:inline-block;padding:", ";", ";color:var(--", ");background-color:var(--", ");font-size:", ";font-family:var(--secondary-font);line-height:2.5rem;text-decoration:none;text-transform:uppercase;cursor:pointer;white-space:nowrap;transition:color 300ms ease-in-out,background-color 300ms ease-in-out;", ""], (function (t) {
                    return t.$large ? "0 90px" : "0 30px"
                }), (function (t) {
                    return t.$hasmargin && "margin-top: 24px"
                }), (function (t) {
                    return t.$isdark ? "brand-white" : "brand-black"
                }), (function (t) {
                    return t.bgcolor ? t.bgcolor : "brand-black"
                }), (0, c.hO)(18), (function (t) {
                    return (0, l.M)("\n    color: var(--brand-white);\n    background-color: var(--".concat(t.$isdark ? "brand-black" : "gold", ");\n  "))
                }));
            n.Z = function (t) {
                var n = t.children,
                    e = t.href,
                    i = t.isdark,
                    a = t.bgcolor,
                    c = t.large,
                    l = t.hasmargin,
                    u = (0, r.Z)(t, ["children", "href", "isdark", "bgcolor", "large", "hasmargin"]);
                return p(d, e ? (0, o.Z)({
                    href: e,
                    $isdark: i,
                    $large: c,
                    bgcolor: a,
                    $hasmargin: l
                }, u) : (0, o.Z)({
                    as: s.Z,
                    $isdark: i,
                    $large: c,
                    bgcolor: a,
                    $hasmargin: l
                }, u), n)
            }
        },
        7706: function (t, n, e) {
            "use strict";
            var o = e(9163).ZP.div.withConfig({
                displayName: "Container",
                componentId: "sc-4cax2l-0"
            })([]);
            n.Z = o
        },
        2181: function (t, n, e) {
            "use strict";
            var o = e(9163),
                r = e(5697),
                i = e.n(r),
                a = o.ZP.div.withConfig({
                    displayName: "Grid",
                    componentId: "bkwquw-0"
                })(["position:", ";display:grid;grid-column-gap:20px;"], (function (t) {
                    return t.position || "relative"
                }));
            a.propTypes = {
                position: i().oneOf(["static", "relative", "absolute"]),
                styles: i().string
            }, n.Z = a
        },
        8362: function (t, n, e) {
            "use strict";
            var o = e(1229),
                r = e(8347),
                i = e(7294),
                a = e(1664),
                c = i.createElement;
            n.Z = function (t) {
                var n = t.href,
                    e = t.target,
                    i = void 0 === e ? "_blank" : e,
                    l = t.children,
                    s = t.prefetch,
                    u = (0, r.Z)(t, ["href", "target", "children", "prefetch"]),
                    p = new RegExp("https?://(www.)?".concat("andersonsupply.com", "/?"), "g");
                if (!n) return null;
                var d = n.replace(p, "/");
                return d.match(/^(https?:)?\/\/|mailto:/) ? c("a", (0, o.Z)({
                    href: d,
                    target: i,
                    rel: "_blank" === i ? "noreferrer noopener" : void 0
                }, u), l) : c(a.default, {
                    href: d,
                    prefetch: s
                }, c("a", u, l))
            }
        },
        1757: function (t, n, e) {
            "use strict";
            e.d(n, {
                wz: function () {
                    return l
                },
                D0: function () {
                    return s
                },
                Cc: function () {
                    return u
                },
                zi: function () {
                    return p
                },
                FS: function () {
                    return d
                },
                dx: function () {
                    return f
                },
                lC: function () {
                    return m
                },
                Nd: function () {
                    return h
                }
            });
            var o = e(9163),
                r = e(2792),
                i = e(7087),
                a = e(2751),
                c = o.ZP.span.withConfig({
                    displayName: "Jumbo__JumboSpan",
                    componentId: "sc-1m2nqo-0"
                })(["display:block;font-family:var(--secondary-font);text-transform:uppercase;"]),
                l = ((0, o.ZP)(c).withConfig({
                    displayName: "Jumbo__FourHundred",
                    componentId: "sc-1m2nqo-1"
                })(["font-size:", ";line-height:.93;", " ", ""], (0, i.hO)(400), r.Z.below(a.bp.desktopSm, "\n    font-size: ".concat((0, i.hO)(300), ";\n  ")), r.Z.below(a.bp.laptopSm, "\n    font-size: ".concat((0, i.hO)(200), ";\n  "))), (0, o.ZP)(c).withConfig({
                    displayName: "Jumbo__TwoTen",
                    componentId: "sc-1m2nqo-2"
                })(["font-size:", ";", ""], (0, i.hO)(210), r.Z.below(a.bp.desktopSm, "\n    font-size: ".concat((0, i.hO)(156), ";\n  ")))),
                s = (0, o.ZP)(c).withConfig({
                    displayName: "Jumbo__OneSixty",
                    componentId: "sc-1m2nqo-3"
                })(["font-size:", ";line-height:1.0625;", " ", ""], (0, i.hO)(160), r.Z.below(a.bp.desktopSm, "\n    font-size: ".concat((0, i.hO)(120), ";\n  ")), r.Z.below(a.bp.portrait, "\n    font-size: ".concat((0, i.hO)(68), ";\n  "))),
                u = (0, o.ZP)(c).withConfig({
                    displayName: "Jumbo__OneThirty",
                    componentId: "sc-1m2nqo-4"
                })(["font-size:", ";", " ", ""], (0, i.hO)(130), r.Z.below(a.bp.desktopSm, "\n    font-size: ".concat((0, i.hO)(100), ";\n  ")), r.Z.below(a.bp.portrait, "\n    font-size: ".concat((0, i.hO)(65), ";\n  "))),
                p = (0, o.ZP)(c).withConfig({
                    displayName: "Jumbo__Ninety",
                    componentId: "sc-1m2nqo-5"
                })(["font-size:", ";line-height:1;", ""], (0, i.hO)(90), r.Z.below(a.bp.desktopSm, "\n    font-size: ".concat((0, i.hO)(68), ";\n  "))),
                d = (0, o.ZP)(c).withConfig({
                    displayName: "Jumbo__Sixty",
                    componentId: "sc-1m2nqo-6"
                })(["font-size:", ";line-height:1;"], (0, i.hO)(60)),
                f = (0, o.ZP)(c).withConfig({
                    displayName: "Jumbo__Forty",
                    componentId: "sc-1m2nqo-7"
                })(["font-size:", ";", ""], (0, i.hO)(40), r.Z.below(a.bp.mobile, "\n    font-size: ".concat((0, i.hO)(28), ";\n  "))),
                m = (0, o.ZP)(c).withConfig({
                    displayName: "Jumbo__TwentyFive",
                    componentId: "sc-1m2nqo-8"
                })(["font-size:", ";"], (0, i.hO)(25)),
                h = (0, o.ZP)(c).withConfig({
                    displayName: "Jumbo__Fifteen",
                    componentId: "sc-1m2nqo-9"
                })(["font-size:", ";"], (0, i.hO)(15))
        },
        845: function (t, n, e) {
            "use strict";
            var o = e(1229),
                r = e(4121),
                i = e(8347),
                a = e(7294),
                c = e(131),
                l = e(2386),
                s = a.createElement,
                u = function (t) {
                    if (t) return t.map((function (t) {
                        return {
                            media: t.media,
                            width: t.width,
                            height: t.height
                        }
                    }))
                };
            n.Z = function (t) {
                var n = t.src,
                    e = t.srcSet,
                    a = t.picture,
                    p = t.width,
                    d = t.height,
                    f = t.className,
                    m = t.bottomOffset,
                    h = (0, i.Z)(t, ["src", "srcSet", "picture", "width", "height", "className", "bottomOffset"]),
                    g = (0, c.YD)({
                        triggerOnce: !0,
                        rootMargin: m || "500px"
                    }),
                    b = (0, r.Z)(g, 2),
                    v = b[0],
                    y = b[1];
                return s(l.Z, (0, o.Z)({
                    width: p,
                    height: d,
                    className: f,
                    src: y ? n : void 0,
                    srcSet: y ? e : void 0,
                    picture: y ? a : u(a),
                    innerRef: v
                }, h))
            }
        },
        4640: function (t, n, e) {
            "use strict";
            e.d(n, {
                dd: function () {
                    return g
                }
            });
            var o = e(1229),
                r = e(8347),
                i = e(4121),
                a = e(4671),
                c = e(7294),
                l = e(3253),
                s = e.n(l),
                u = e(9163),
                p = e(7087),
                d = e(7706),
                f = e(2334),
                m = c.createElement,
                h = (0, a.Z)((function (t) {
                    return {
                        modalOpen: !1,
                        modalData: null,
                        toggleModal: function () {
                            var n = arguments.length > 0 && void 0 !== arguments[0] && arguments[0],
                                e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : null;
                            return t(n ? function (t) {
                                return {
                                    modalOpen: t.modalOpen !== n && n,
                                    modalData: e
                                }
                            } : {
                                modalOpen: !1,
                                modalData: e
                            })
                        }
                    }
                })),
                g = (0, i.Z)(h, 1)[0],
                b = (0, u.ZP)(f.Z).withConfig({
                    displayName: "GullsModal__Close",
                    componentId: "xre353-0"
                })(["", " ", " background-color:var(--gold);font-size:1.5rem;line-height:44px;border-radius:50%;transition:all .3s;"], (0, p.dp)(44), (0, p.FK)("fixed", 20, 20, null, null));
            n.ZP = function (t) {
                var n = t.className,
                    e = t.appElement,
                    i = void 0 === e ? "#__next" : e,
                    a = t.children,
                    c = (0, r.Z)(t, ["className", "appElement", "children"]),
                    l = g(),
                    u = l.modalOpen,
                    p = l.toggleModal;
                return s().setAppElement(i), m(s(), (0, o.Z)({
                    isOpen: !!u,
                    onRequestClose: function () {
                        p()
                    },
                    overlayClassName: "Overlay",
                    className: "Modal",
                    portalClassName: n
                }, c), m(b, {
                    "aria-label": "close modal",
                    onClick: function () {
                        p()
                    }
                }, "\xd7"), m(d.Z, null, a))
            }
        },
        8130: function (t, n, e) {
            "use strict";
            var o = e(7294),
                r = e(9163),
                i = e(2751),
                a = o.createElement,
                c = r.ZP.div.withConfig({
                    displayName: "YouTubeModal__VidWrap",
                    componentId: "sc-1hjiljy-0"
                })(["overflow:hidden;padding-bottom:56.25%;position:relative;height:0;iframe{height:100%;width:100%;max-height:90vh;", "}"], (0, i.D)());
            n.Z = function (t) {
                var n = t.youTubeID;
                return a(c, null, a("iframe", {
                    src: "https://www.youtube.com/embed/".concat(n, "?rel=0&autoplay=1"),
                    frameBorder: "0",
                    allow: "accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture",
                    allowFullScreen: !0
                }))
            }
        },
        1407: function (t, n, e) {
            "use strict";
            var o = e(9163),
                r = e(4640),
                i = (0, o.F4)(["0%{background-color:rgba(0,0,0,0);}100%{background-color:rgba(0,0,0,.75);}"]),
                a = (0, o.ZP)(r.ZP).withConfig({
                    displayName: "Modal",
                    componentId: "sc-1ezkd1n-0"
                })([".Overlay{z-index:200;display:flex;justify-content:center;align-items:center;position:fixed;top:0;right:0;bottom:0;left:0;background-color:rgba(0,0,0,.75);animation:1.2s ", " ease-in;}.Modal{position:relative;width:100%;border:0;outline:none;text-align:center;pointer-events:none;> div{pointer-events:auto;}}"], i);
            n.Z = a
        },
        2430: function (t, n, e) {
            "use strict";
            var o = e(1229),
                r = e(8347),
                i = e(7294),
                a = e(9163),
                c = e(2751),
                l = e(2334),
                s = i.createElement,
                u = (0, a.ZP)(l.Z).withConfig({
                    displayName: "PlayButton__StyledPlayButton",
                    componentId: "uz6a6t-0"
                })(["position:relative;min-width:68px;min-height:68px;color:var(--brand-white);background-color:var(--gold);border-radius:50%;transition:color 300ms ease-in-out,background-color 300ms ease-in-out;svg{position:absolute;top:50%;left:50%;width:12px;height:14px;fill:currentColor;transform:translate(-50%,-50%);}", ""], (0, c.M)("\n    color: var(--brand-black);\n    background-color: var(--brand-white);\n  "));
            n.Z = function (t) {
                var n = t.label,
                    e = t.className,
                    i = t.playing,
                    a = (0, r.Z)(t, ["label", "className", "playing"]);
                return s(u, (0, o.Z)({
                    "aria-label": n,
                    className: e,
                    "aria-pressed": i
                }, a), i ? s("svg", {
                    width: "13",
                    height: "18",
                    viewBox: "0 0 13 18",
                    xmlns: "http://www.w3.org/2000/svg"
                }, s("path", {
                    d: "M0 0h4v18H0V0zm9 0h4v18H9V0z",
                    fill: "currentColor"
                })) : s("svg", {
                    width: "12",
                    height: "14",
                    viewBox: "0 0 60 60",
                    xmlns: "http://www.w3.org/2000/svg"
                }, s("polygon", {
                    points: "10 0 60 30 60 30 10 60"
                }), s("polygon", {
                    points: "10 20 50 30 50 30 10 40"
                })))
            }
        },
        2386: function (t, n, e) {
            "use strict";
            var o = e(1229),
                r = e(6265),
                i = e(8347),
                a = e(7294),
                c = e(9163),
                l = e(7087),
                s = e(4477),
                u = a.createElement;

            function p(t, n) {
                var e = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var o = Object.getOwnPropertySymbols(t);
                    n && (o = o.filter((function (n) {
                        return Object.getOwnPropertyDescriptor(t, n).enumerable
                    }))), e.push.apply(e, o)
                }
                return e
            }

            function d(t) {
                for (var n = 1; n < arguments.length; n++) {
                    var e = null != arguments[n] ? arguments[n] : {};
                    n % 2 ? p(Object(e), !0).forEach((function (n) {
                        (0, r.Z)(t, n, e[n])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(e)) : p(Object(e)).forEach((function (n) {
                        Object.defineProperty(t, n, Object.getOwnPropertyDescriptor(e, n))
                    }))
                }
                return t
            }
            var f = function (t) {
                    var n = t.pictures,
                        e = t.children;
                    return n ? u("picture", null, n.map((function (t) {
                        return u("source", {
                            srcSet: t.srcSet,
                            media: t.media,
                            type: t.type,
                            key: t.media
                        })
                    })), e) : e
                },
                m = c.ZP.img.withConfig({
                    displayName: "RatioImg__Img",
                    componentId: "xinq7a-0"
                })(["", ""], (function (t) {
                    return t.styles && (0, c.iv)(["&&{", "}"], t.styles)
                })),
                h = c.ZP.div.withConfig({
                    displayName: "RatioImg__Wrapper",
                    componentId: "xinq7a-1"
                })(["position:relative;&::after,", "{display:block;}", ""], m, (function (t) {
                    return t.width && t.height && (0, c.iv)(['&::after{content:"";width:100%;padding-bottom:var(--aspect-ratio);', "}", "{", " ", " object-fit:cover;}"], t.picture && (0, c.iv)(["", ""], function (t) {
                        if (t) return t.map((function (t) {
                            return t.media && t.width && t.height ? "\n      @media".concat(t.media, " {\n        padding-bottom: ").concat((0, s.Z)(t.height, t.width), "\n      }\n    ") : ""
                        }))
                    }(t.picture)), m, (0, l.dp)("100%"), (0, l.v8)())
                }));
            n.Z = function (t) {
                var n = t.src,
                    e = t.srcSet,
                    r = t.picture,
                    a = t.width,
                    c = t.height,
                    l = t.className,
                    p = t.innerRef,
                    g = t.imageStyles,
                    b = t.style,
                    v = t.as,
                    y = (0, i.Z)(t, ["src", "srcSet", "picture", "width", "height", "className", "innerRef", "imageStyles", "style", "as"]);
                return u(h, {
                    width: a,
                    height: c,
                    className: l,
                    picture: r,
                    ref: p,
                    as: v,
                    style: d({
                        "--aspect-ratio": (0, s.Z)(c, a)
                    }, b)
                }, u(f, {
                    pictures: r
                }, u(m, (0, o.Z)({
                    src: n,
                    srcSet: e,
                    styles: g
                }, y))))
            }
        },
        8092: function (t, n, e) {
            "use strict";
            // console.log(t.exports);
            var o = e(9163).ZP.section.withConfig({
                displayName: "Section",
                componentId: "sc-1l8379i-0"
            })((function (t) {
                return null
            }));
            n.Z = o
        },
        9544: function (t, n, e) {
            "use strict";
            var o = e(1229),
                r = e(8347),
                i = e(7294),
                a = e(9163),
                c = e(5833),
                l = e(2751),
                s = e(1432),
                u = e(8362),
                p = i.createElement,
                d = (0, a.ZP)(u.Z).withConfig({
                    displayName: "TextBlock__StyledLink",
                    componentId: "sc-5igzez-0"
                })(['color:var(--gold);text-decoration:none;position:relative;transition:color 300ms ease;&::after{content:"";width:100%;height:2px;background:var(--brand-black);position:absolute;left:0;bottom:-1px;transition:color 300ms ease;}', ""], (0, l.M)("\n    color: var(--brand-black);\n\n    &::after {\n      color: var(--gold);\n    }\n  ")),
                f = function (t, n, e) {
                    return p(d, {
                        key: "".concat(n.data.link_type).concat(n.start),
                        href: (0, s.kG)(n.data)
                    }, e)
                };
            n.Z = function (t) {
                var n = t.content,
                    e = (0, r.Z)(t, ["content"]);
                return n ? p(c.RichText, (0, o.Z)({
                    render: n,
                    serializeHyperlink: f
                }, e)) : null
            }
        },
        2334: function (t, n, e) {
            "use strict";
            var o = e(9163).ZP.button.withConfig({
                displayName: "UnstyledButton",
                componentId: "sc-1h2eotw-0"
            })(["padding:0;font-size:inherit;font-weight:inherit;font-family:inherit;appearance:none;background-color:transparent;border:0;text-align:inherit;"]);
            n.Z = o
        },
        4356: function (t, n, e) {
            "use strict";
            var o = e(9163),
                r = e(5697),
                i = e.n(r),
                a = o.ZP.ul.withConfig({
                    displayName: "UnstyledList",
                    componentId: "nh8fku-0"
                })(["list-style:none;padding:0;margin:0;"]);
            a.Item = o.ZP.li.withConfig({
                displayName: "UnstyledList__Item",
                componentId: "nh8fku-1"
            })(["display:", ";"], (function (t) {
                return t.display || "inline-block"
            })), a.propTypes = {
                styles: i().string
            }, a.Item.propTypes = {
                display: i().string
            }, n.Z = a
        },
        2751: function (t, n, e) {
            "use strict";
            e.d(n, {
                bp: function () {
                    return o
                },
                M: function () {
                    return r
                },
                D: function () {
                    return i
                }
            });
            var o = {
                    desktopLg: "1800px",
                    desktop: "1680px",
                    desktopSm: "1440px",
                    laptop: "1380px",
                    laptopSm: "1240px",
                    tablet: "1024px",
                    portrait: "880px",
                    mobile: "767px",
                    mobileMid: "625px",
                    mobileSm: "580px",
                    mobileRealSm: "375px"
                },
                r = function (t) {
                    return "\n  @media(hover: hover) {\n    &:hover {\n      ".concat(t, "\n    }\n  }\n")
                },
                i = function () {
                    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
                        n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 0,
                        e = "",
                        o = "";
                    return null !== t && (e = "left: ".concat(50 + t, "%;"), o = "translateX(-50%)"), null !== n && (e = " ".concat(e, " top: ").concat(50 + n, "%;"), o = "".concat(o, " translateY(-50%)")), "\n    position: absolute;\n    ".concat(e, "\n    transform: ").concat(o, ";\n  ")
                }
        },
        4477: function (t, n) {
            "use strict";
            n.Z = function (t, n) {
                var e = arguments.length > 2 && void 0 !== arguments[2] && arguments[2],
                    o = t / n * 100;
                return "".concat(e ? Math.round(10 * o / 10) : o, "%")
            }
        },
        1432: function (t, n, e) {
            "use strict";
            e.d(n, {
                KU: function () {
                    return u
                },
                kG: function () {
                    return l
                }
            });
            var o = e(6265),
                r = e(9458),
                i = e(4155).env.PRISMIC_API;

            function a(t, n) {
                var e = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var o = Object.getOwnPropertySymbols(t);
                    n && (o = o.filter((function (n) {
                        return Object.getOwnPropertyDescriptor(t, n).enumerable
                    }))), e.push.apply(e, o)
                }
                return e
            }

            function c(t) {
                for (var n = 1; n < arguments.length; n++) {
                    var e = null != arguments[n] ? arguments[n] : {};
                    n % 2 ? a(Object(e), !0).forEach((function (n) {
                        (0, o.Z)(t, n, e[n])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(e)) : a(Object(e)).forEach((function (n) {
                        Object.defineProperty(t, n, Object.getOwnPropertyDescriptor(e, n))
                    }))
                }
                return t
            }
            var l = function (t) {
                    if ("Document" === t.link_type) switch (t.type) {
                        case "services_single":
                            return "/services/".concat(t.uid);
                        default:
                            return "/".concat(t.type)
                    }
                    return t.url
                },
                s = function () {
                    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : null,
                        n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : null,
                        e = t ? {
                            req: t
                        } : {},
                        o = n ? {
                            accessToken: n
                        } : {};
                    return c(c({}, e), o)
                },
                u = function () {
                    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : null;
                    return r.Z.client(i, s(t, ""))
                }
        }
    }
]);