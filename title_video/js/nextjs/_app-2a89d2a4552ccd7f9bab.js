(self.webpackChunk_N_E = self.webpackChunk_N_E || []).push([
    [888], {
        9520: function (n, t, e) {
            "use strict";
            var o = e(1229),
                r = e(8347),
                i = e(7294),
                a = e(9163),
                l = e(7087),
                C = e(2751),
                c = e(2334),
                s = e(8362),
                u = i.createElement,
                d = (0, a.ZP)(s.Z).withConfig({
                    displayName: "Button__StyledButton",
                    componentId: "sc-1webfnb-0"
                })(["display:inline-block;padding:", ";", ";color:var(--", ");background-color:var(--", ");font-size:", ";font-family:var(--secondary-font);line-height:2.5rem;text-decoration:none;text-transform:uppercase;cursor:pointer;white-space:nowrap;transition:color 300ms ease-in-out,background-color 300ms ease-in-out;", ""], (function (n) {
                    return n.$large ? "0 90px" : "0 30px"
                }), (function (n) {
                    return n.$hasmargin && "margin-top: 24px"
                }), (function (n) {
                    return n.$isdark ? "brand-white" : "brand-black"
                }), (function (n) {
                    return n.bgcolor ? n.bgcolor : "brand-black"
                }), (0, l.hO)(18), (function (n) {
                    return (0, C.M)("\n    color: var(--brand-white);\n    background-color: var(--".concat(n.$isdark ? "brand-black" : "gold", ");\n  "))
                }));
            t.Z = function (n) {
                var t = n.children,
                    e = n.href,
                    i = n.isdark,
                    a = n.bgcolor,
                    l = n.large,
                    C = n.hasmargin,
                    s = (0, r.Z)(n, ["children", "href", "isdark", "bgcolor", "large", "hasmargin"]);
                return u(d, e ? (0, o.Z)({
                    href: e,
                    $isdark: i,
                    $large: l,
                    bgcolor: a,
                    $hasmargin: C
                }, s) : (0, o.Z)({
                    as: c.Z,
                    $isdark: i,
                    $large: l,
                    bgcolor: a,
                    $hasmargin: C
                }, s), t)
            }
        },
        7706: function (n, t, e) {
            "use strict";
            var o = e(9163).ZP.div.withConfig({
                displayName: "Container",
                componentId: "sc-4cax2l-0"
            })(["max-width:var(--container-width);margin:auto;"]);
            t.Z = o
        },
        2181: function (n, t, e) {
            "use strict";
            var o = e(9163),
                r = e(5697),
                i = e.n(r),
                a = o.ZP.div.withConfig({
                    displayName: "Grid",
                    componentId: "bkwquw-0"
                })([]);
            a.propTypes = {
                position: i().oneOf(["static", "relative", "absolute"]),
                styles: i().string
            }, t.Z = a
        },
        8362: function (n, t, e) {
            "use strict";
            var o = e(1229),
                r = e(8347),
                i = e(7294),
                a = e(1664),
                l = i.createElement;
            t.Z = function (n) {
                var t = n.href,
                    e = n.target,
                    i = void 0 === e ? "_blank" : e,
                    C = n.children,
                    c = n.prefetch,
                    s = (0, r.Z)(n, ["href", "target", "children", "prefetch"]),
                    u = new RegExp("https?://(www.)?".concat("andersonsupply.com", "/?"), "g");
                if (!t) return null;
                var d = t.replace(u, "/");
                return d.match(/^(https?:)?\/\/|mailto:/) ? l("a", (0, o.Z)({
                    href: d,
                    target: i,
                    rel: "_blank" === i ? "noreferrer noopener" : void 0
                }, s), C) : l(a.default, {
                    href: d,
                    prefetch: c
                }, l("a", s, C))
            }
        },
        1757: function (n, t, e) {
            "use strict";
            e.d(t, {
                wz: function () {
                    return C
                },
                D0: function () {
                    return c
                },
                Cc: function () {
                    return s
                },
                zi: function () {
                    return u
                },
                FS: function () {
                    return d
                },
                dx: function () {
                    return p
                },
                lC: function () {
                    return f
                },
                Nd: function () {
                    return m
                }
            });
        },
        845: function (n, t, e) {
            "use strict";
            var o = e(1229),
                r = e(4121),
                i = e(8347),
                a = e(7294),
                l = e(131),
                C = e(2386),
                c = a.createElement,
                s = function (n) {
                    if (n) return n.map((function (n) {
                        return {
                            media: n.media,
                            width: n.width,
                            height: n.height
                        }
                    }))
                };
            t.Z = function (n) {
                var t = n.src,
                    e = n.srcSet,
                    a = n.picture,
                    u = n.width,
                    d = n.height,
                    p = n.className,
                    f = n.bottomOffset,
                    m = (0, i.Z)(n, ["src", "srcSet", "picture", "width", "height", "className", "bottomOffset"]),
                    h = (0, l.YD)({
                        triggerOnce: !0,
                        rootMargin: f || "500px"
                    }),
                    v = (0, r.Z)(h, 2),
                    g = v[0],
                    b = v[1];
                return c(C.Z, (0, o.Z)({
                    width: u,
                    height: d,
                    className: p,
                    src: b ? t : void 0,
                    srcSet: b ? e : void 0,
                    picture: b ? a : s(a),
                    innerRef: g
                }, m))
            }
        },
        2386: function (n, t, e) {
            "use strict";
            var o = e(1229),
                r = e(6265),
                i = e(8347),
                a = e(7294),
                c = e(4477),
                s = a.createElement;

            function u(n, t) {
                var e = Object.keys(n);
                if (Object.getOwnPropertySymbols) {
                    var o = Object.getOwnPropertySymbols(n);
                    t && (o = o.filter((function (t) {
                        return Object.getOwnPropertyDescriptor(n, t).enumerable
                    }))), e.push.apply(e, o)
                }
                return e
            }

            function d(n) {
                for (var t = 1; t < arguments.length; t++) {
                    var e = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? u(Object(e), !0).forEach((function (t) {
                        (0, r.Z)(n, t, e[t])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(n, Object.getOwnPropertyDescriptors(e)) : u(Object(e)).forEach((function (t) {
                        Object.defineProperty(n, t, Object.getOwnPropertyDescriptor(e, t))
                    }))
                }
                return n
            }
            t.Z = function (n) {
                var t = n.src,
                    e = n.srcSet,
                    r = n.picture,
                    a = n.width,
                    l = n.height,
                    C = n.className,
                    u = n.innerRef,
                    h = n.imageStyles,
                    v = n.style,
                    g = n.as,
                    b = (0, i.Z)(n, ["src", "srcSet", "picture", "width", "height", "className", "innerRef", "imageStyles", "style", "as"]);
                return s(m, {
                    width: a,
                    height: l,
                    className: C,
                    picture: r,
                    ref: u,
                    as: g,
                    style: d({
                        "--aspect-ratio": (0, c.Z)(l, a)
                    }, v)
                }, s(p, {
                    pictures: r
                }, s(f, (0, o.Z)({
                    src: t,
                    srcSet: e,
                    styles: h
                }, b))))
            }
        },
        2334: function (n, t, e) {
            "use strict";
            var o = e(9163).ZP.button.withConfig({
                displayName: "UnstyledButton",
                componentId: "sc-1h2eotw-0"
            })(["padding:0;font-size:inherit;font-weight:inherit;font-family:inherit;appearance:none;background-color:transparent;border:0;text-align:inherit;"]);
            t.Z = o
        },
        425: function (n, t, e) {
            "use strict";
            e.r(t), e.d(t, {
                default: function () {
                    return Yn
                }
            });
            var o = e(809),
                r = e.n(o),
                i = e(6265),
                a = e(2447),
                l = e(1229),
                C = e(5988),
                c = e(7294),
                s = e(7544),
                u = e(9458),
                d = e(1432),
                p = e(4593),
                f = e(1163),
                m = e(7261),
                h = e(9163),
                v = e(2792),
                g = e(2751);

            function b() {
                var n = (0, m.Z)(['\n\n  :root {\n    --container-width: 1680px;\n    --container-gutter: calc((100vw - var(--container-width)) / 2);\n    --customer-container-width: 1396px;\n    --customer-container-gutter: calc(((100vw - var(--customer-container-width)) / 2) - var(--container-gutter));\n    --white: #fff;\n    --black: #16191a;\n    --brand-black: #282829;\n    --brand-white: #f0eee3;\n    --gold: #b48645;\n    --light-gray: #f4f4f4;\n    --border-color: rgba(240, 238, 227, .2);\n    --spacing: 170px;\n    --primary-font: "GothamSS", sans-serif;\n    --secondary-font: "Anton", sans-serif;\n    --focus-outline: 1px solid var(--gold);\n\n    ', "\n\n    ", "\n\n    ", "\n\n    ", "\n\n    ", "\n\n    ", "\n\n    ", "\n\n    ", "\n\n    ", "\n  }\n\n  *,\n  *::before,\n  *::after { box-sizing: border-box; }\n\n  body {\n    margin: 0;\n    font-family: var(--primary-font);\n    font-size: 1rem;\n    font-weight: 400;\n    line-height: 1.75;\n    color: var(--brand-black);\n  }\n\n  h1,\n  h2,\n  h3,\n  h4,\n  h5,\n  h6 {\n    margin: 0;\n    font-family: var(--secondary-font);\n    font-weight: 400;\n    line-height: 1;\n    text-transform: uppercase;\n    color: inherit;\n  }\n\n  h2 { margin-bottom: 36px; }\n\n  img { max-width: 100%; }\n\n  blockquote,\n  figure { margin: 0; }\n\n  form { position: relative; }\n\n  a {\n    color: inherit;\n    text-decoration: none;\n  }\n\n  button,\n  a {\n    outline: none;\n\n    &::-moz-focus-inner { border: 0; }\n\n    &:not(:disabled) { cursor: pointer; }\n  }\n\n  /* for page transitions */\n\n  .transitioning {\n    width: 100%;\n    height: 100vh;\n    overflow: hidden;\n    position: relative;\n    background: var(--gold);\n  }\n"]);
                return b = function () {
                    return n
                }, n
            }
            var w = (0, h.vJ)(b(), v.Z.below(g.bp.desktopLg, "\n      --container-width: 1440px;\n      --spacing: 128px;\n    "), v.Z.below(g.bp.desktop, "\n      --container-width: 1280px;\n      --spacing: 85px;\n    "), v.Z.below(g.bp.laptop, "\n      --container-width: 1080px;\n    "), v.Z.below(g.bp.laptopSm, "\n      --container-width: 940px;\n    "), v.Z.below(g.bp.tablet, "\n      --container-width: 768px;\n    "), v.Z.below(g.bp.portrait, "\n      --container-width: 700px;\n    "), v.Z.below(g.bp.mobile, "\n      --container-width: 600px;\n    "), v.Z.below(g.bp.mobileMid, "\n      --container-width: 500px;\n    "), v.Z.below(g.bp.mobileSm, "\n      --container-width: 335px;\n    "));

            var P = e(6358),
                an = e(4537),
                ln = e(8983),
                sn = c.createElement,
                un = 1e3,
                dn = (0, h.F4)(["0%{transform:scale(1);filter:saturate(1)  sepia(0);}30%{transform:scale(.6);}50%{filter:saturate(0) sepia(.1);}70%{transform:scale(.6);}100%{transform:scale(1);filter:saturate(1)  sepia(0);}"]),
                pn = (0, h.F4)(["from{transform:translateX(0);}to{transform:translateX(-100%);}"]),
                fn = (0, h.F4)(["from{transform:translateX(100%);}to{transform:translateX(0);}"]),
                mn = h.ZP.div.withConfig({
                    displayName: "PageTransition__MainWrapper",
                    componentId: "sc-1v8qt40-0"
                })(["animation-fill-mode:both;&.page-enter-active,&.page-enter{position:absolute;top:0;left:0;width:100%;z-index:4;}&.page-enter-active,&.page-exit-active{.page-transition-inner{height:100vh;overflow:hidden;box-shadow:0 0 0 30px var(--light-gray),0 30px 60px rgba(0,0,0,.4);animation:", "ms ", " cubic-bezier(.45,0,.55,1);animation-fill-mode:both;}}&.page-enter-active{animation:", "ms ", " ", "ms cubic-bezier(.37,0,.63,1);animation-fill-mode:both;}&.page-exit-active{z-index:5;position:relative;animation:", "ms ", " ", "ms cubic-bezier(.37,0,.63,1);animation-fill-mode:both;main,footer{transform:translateY(-", "px);}}"], un, dn, 500, fn, 250, 500, pn, 250, (function (n) {
                    return n.routingPageOffset
                })),
                hn = h.ZP.div.withConfig({
                    displayName: "PageTransition__SecondaryWrapper",
                    componentId: "sc-1v8qt40-1"
                })(["position:relative;z-index:0;background:var(--white);"]),
                vn = function (n) {
                    var t = n.route,
                        e = n.children,
                        o = n.routingPageOffset,
                        r = (0, c.useState)(),
                        i = r[0],
                        a = r[1];
                    return sn(an.Z, {
                        className: i ? "transitioning" : ""
                    }, sn(ln.Z, {
                        timeout: un,
                        classNames: "page",
                        key: t,
                        onEnter: function () {
                            a(!0)
                        },
                        onExited: function () {
                            a(), P.ZP.registerPlugin(cn()), cn().refresh(!0)
                        }
                    }, sn(mn, {
                        routingPageOffset: o
                    }, sn(hn, {
                        className: "page-transition-inner"
                    }, e))))
                },
                Rn = c.createElement;

            function Hn(n, t) {
                var e = Object.keys(n);
                if (Object.getOwnPropertySymbols) {
                    var o = Object.getOwnPropertySymbols(n);
                    t && (o = o.filter((function (t) {
                        return Object.getOwnPropertyDescriptor(n, t).enumerable
                    }))), e.push.apply(e, o)
                }
                return e
            }

            function Dn(n) {
                for (var t = 1; t < arguments.length; t++) {
                    var e = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? Hn(Object(e), !0).forEach((function (t) {
                        (0, i.Z)(n, t, e[t])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(n, Object.getOwnPropertyDescriptors(e)) : Hn(Object(e)).forEach((function (t) {
                        Object.defineProperty(n, t, Object.getOwnPropertyDescriptor(e, t))
                    }))
                }
                return n
            }
            var Jn = function (n) {
                var B = n.Component,
                    G = n.pageProps,
                    H = (0, f.useRouter)(),
                    D = (0, c.useState)(0),
                    J = D[0],
                    Y = D[1],
                    X = function () {
                        return "/" === H.asPath ? "/" : H.asPath.replace(/\/$/, "")
                    };
                (0, c.useEffect)((function () {
                    var n = function () {
                        Y(window.scrollY)
                    };
                    return H.events.on("routeChangeStart", n),
                        function () {
                            H.events.off("routeChangeStart", n)
                        }
                }), [H.events, Y, H.asPath]);
                return Rn(c.Fragment, null, Rn(p.q, {
                }), Rn(vn, {
                    routingPageOffset: J,
                    route: X()
                }, Rn(B, (0, l.Z)({
                    key: H.asPath
                }, G, {
                    className: "jsx-1060907307 " + (G && null != G.className && G.className || "")
                }))), Rn(w, null), Rn(C.default, {
                    id: "1060907307"
                }, []))
            };
            Jn.getInitialProps = function () {
                var n = (0, a.Z)(r().mark((function n(t) {
                    var e, o, i, a;
                    return r().wrap((function (n) {
                        for (;;) switch (n.prev = n.next) {
                            case 0:
                                return n.next = 2, s.default.getInitialProps(t);
                            case 2:
                                return e = n.sent, o = (0, d.KU)(), i = ["YJQrvBEAACEAGlk2", "YJRHIREAACEAGtZB", "YIg9ABAAACMAMUpk"], n.next = 7, o.query(u.Z.Predicates.in("document.id", i));
                            case 7:
                                return a = n.sent, n.abrupt("return", Dn(Dn({}, e), {}, {
                                    pageData: a
                                }));
                            case 9:
                            case "end":
                                return n.stop()
                        }
                    }), n)
                })));
                return function (t) {
                    return n.apply(this, arguments)
                }
            }();
            var Yn = Jn
        },
        2751: function (n, t, e) {
            "use strict";
            e.d(t, {
                bp: function () {
                    return o
                },
                M: function () {
                    return r
                },
                D: function () {
                    return i
                }
            });
            var o = {
                    desktopLg: "1800px",
                    desktop: "1680px",
                    desktopSm: "1440px",
                    laptop: "1380px",
                    laptopSm: "1240px",
                    tablet: "1024px",
                    portrait: "880px",
                    mobile: "767px",
                    mobileMid: "625px",
                    mobileSm: "580px",
                    mobileRealSm: "375px"
                },
                r = function (n) {
                    return "\n  @media(hover: hover) {\n    &:hover {\n      ".concat(n, "\n    }\n  }\n")
                },
                i = function () {
                    var n = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
                        t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 0,
                        e = "",
                        o = "";
                    return null !== n && (e = "left: ".concat(50 + n, "%;"), o = "translateX(-50%)"), null !== t && (e = " ".concat(e, " top: ").concat(50 + t, "%;"), o = "".concat(o, " translateY(-50%)")), "\n    position: absolute;\n    ".concat(e, "\n    transform: ").concat(o, ";\n  ")
                }
        },
        4477: function (n, t) {
            "use strict";
            t.Z = function (n, t) {
                var e = arguments.length > 2 && void 0 !== arguments[2] && arguments[2],
                    o = n / t * 100;
                return "".concat(e ? Math.round(10 * o / 10) : o, "%")
            }
        },
        1432: function (n, t, e) {
            "use strict";
            e.d(t, {
                KU: function () {
                    return s
                },
                kG: function () {
                    return C
                }
            });
            var o = e(6265),
                r = e(9458),
                i = e(4155).env.PRISMIC_API;

            function a(n, t) {
                var e = Object.keys(n);
                if (Object.getOwnPropertySymbols) {
                    var o = Object.getOwnPropertySymbols(n);
                    t && (o = o.filter((function (t) {
                        return Object.getOwnPropertyDescriptor(n, t).enumerable
                    }))), e.push.apply(e, o)
                }
                return e
            }

            function l(n) {
                for (var t = 1; t < arguments.length; t++) {
                    var e = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? a(Object(e), !0).forEach((function (t) {
                        (0, o.Z)(n, t, e[t])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(n, Object.getOwnPropertyDescriptors(e)) : a(Object(e)).forEach((function (t) {
                        Object.defineProperty(n, t, Object.getOwnPropertyDescriptor(e, t))
                    }))
                }
                return n
            }
            var C = function (n) {
                    if ("Document" === n.link_type) switch (n.type) {
                        case "services_single":
                            return "/services/".concat(n.uid);
                        default:
                            return "/".concat(n.type)
                    }
                    return n.url
                },
                c = function () {
                    var n = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : null,
                        t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : null,
                        e = n ? {
                            req: n
                        } : {},
                        o = t ? {
                            accessToken: t
                        } : {};
                    return l(l({}, e), o)
                },
                s = function () {
                    var n = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : null;
                    return r.Z.client(i, c(n, ""))
                }
        },
        1780: function (n, t, e) {
            (window.__NEXT_P = window.__NEXT_P || []).push(["/_app", function () {
                return e(425)
            }])
        }
    },
    function (n) {
        var t = function (t) {
            return n(n.s = t)
        };
        n.O(0, [774, 351, 663, 46, 258, 562], (function () {
            return t(1780), t(2441)
        }));
        var e = n.O();
        _N_E = e
    }
]);