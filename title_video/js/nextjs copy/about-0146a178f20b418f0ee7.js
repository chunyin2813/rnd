(self.webpackChunk_N_E = self.webpackChunk_N_E || []).push([
    [521], {
        9664: function (t, e) {
            ! function (t) {
                "use strict";
                var e, n, i, o, l, r, a, c = function () {
                        return "undefined" !== typeof window
                    },
                    d = function () {
                        return e || c() && (e = window.gsap) && e.registerPlugin && e
                    },
                    u = function (t) {
                        return "string" === typeof t
                    },
                    s = function (t) {
                        return "function" === typeof t
                    },
                    p = function (t, e) {
                        var n = "x" === e ? "Width" : "Height",
                            r = "scroll" + n,
                            a = "client" + n;
                        return t === i || t === o || t === l ? Math.max(o[r], l[r]) - (i["inner" + n] || o[a] || l[a]) : t[r] - t["offset" + n]
                    },
                    f = function (t, e) {
                        var n = "scroll" + ("x" === e ? "Left" : "Top");
                        return t === i && (null != t.pageXOffset ? n = "page" + e.toUpperCase() + "Offset" : t = null != o[n] ? o : l),
                            function () {
                                return t[n]
                            }
                    },
                    m = function (t, e, n, i) {
                        if (s(t) && (t = t(e, n, i)), "object" !== typeof t) return u(t) && "max" !== t && "=" !== t.charAt(1) ? {
                            x: t,
                            y: t
                        } : {
                            y: t
                        };
                        if (t.nodeType) return {
                            y: t,
                            x: t
                        };
                        var o, l = {};
                        for (o in t) l[o] = "onAutoKill" !== o && s(t[o]) ? t[o](e, n, i) : t[o];
                        return l
                    },
                    g = function (t, e) {
                        if (!(t = r(t)[0]) || !t.getBoundingClientRect) return console.warn("scrollTo target doesn't exist. Using 0") || {
                            x: 0,
                            y: 0
                        };
                        var n = t.getBoundingClientRect(),
                            a = !e || e === i || e === l,
                            c = a ? {
                                top: o.clientTop - (i.pageYOffset || o.scrollTop || l.scrollTop || 0),
                                left: o.clientLeft - (i.pageXOffset || o.scrollLeft || l.scrollLeft || 0)
                            } : e.getBoundingClientRect(),
                            d = {
                                x: n.left - c.left,
                                y: n.top - c.top
                            };
                        return !a && e && (d.x += f(e, "x")(), d.y += f(e, "y")()), d
                    },
                    v = function (t, e, n, i, o) {
                        return isNaN(t) || "object" === typeof t ? u(t) && "=" === t.charAt(1) ? parseFloat(t.substr(2)) * ("-" === t.charAt(0) ? -1 : 1) + i - o : "max" === t ? p(e, n) - o : Math.min(p(e, n), g(t, e)[n] - o) : parseFloat(t) - o
                    },
                    h = function () {
                        e = d(), c() && e && document.body && (i = window, l = document.body, o = document.documentElement, r = e.utils.toArray, e.config({
                            autoKillThreshold: 7
                        }), a = e.config(), n = 1)
                    },
                    y = {
                        version: "3.6.1",
                        name: "scrollTo",
                        rawVars: 1,
                        register: function (t) {
                            e = t, h()
                        },
                        init: function (t, e, o, l, r) {
                            n || h();
                            var a = this;
                            a.isWin = t === i, a.target = t, a.tween = o, e = m(e, l, t, r), a.vars = e, a.autoKill = !!e.autoKill, a.getX = f(t, "x"), a.getY = f(t, "y"), a.x = a.xPrev = a.getX(), a.y = a.yPrev = a.getY(), null != e.x ? (a.add(a, "x", a.x, v(e.x, t, "x", a.x, e.offsetX || 0), l, r), a._props.push("scrollTo_x")) : a.skipX = 1, null != e.y ? (a.add(a, "y", a.y, v(e.y, t, "y", a.y, e.offsetY || 0), l, r), a._props.push("scrollTo_y")) : a.skipY = 1
                        },
                        render: function (t, e) {
                            for (var n, o, l, r, c, d = e._pt, u = e.target, s = e.tween, f = e.autoKill, m = e.xPrev, g = e.yPrev, v = e.isWin; d;) d.r(t, d.d), d = d._next;
                            n = v || !e.skipX ? e.getX() : m, l = (o = v || !e.skipY ? e.getY() : g) - g, r = n - m, c = a.autoKillThreshold, e.x < 0 && (e.x = 0), e.y < 0 && (e.y = 0), f && (!e.skipX && (r > c || r < -c) && n < p(u, "x") && (e.skipX = 1), !e.skipY && (l > c || l < -c) && o < p(u, "y") && (e.skipY = 1), e.skipX && e.skipY && (s.kill(), e.vars.onAutoKill && e.vars.onAutoKill.apply(s, e.vars.onAutoKillParams || []))), v ? i.scrollTo(e.skipX ? n : e.x, e.skipY ? o : e.y) : (e.skipY || (u.scrollTop = e.y), e.skipX || (u.scrollLeft = e.x)), e.xPrev = e.x, e.yPrev = e.y
                        },
                        kill: function (t) {
                            var e = "scrollTo" === t;
                            (e || "scrollTo_x" === t) && (this.skipX = 1), (e || "scrollTo_y" === t) && (this.skipY = 1)
                        }
                    };
                y.max = p, y.getOffset = g, y.buildGetter = f, d() && e.registerPlugin(y), t.ScrollToPlugin = y, t.default = y, Object.defineProperty(t, "__esModule", {
                    value: !0
                })
            }(e)
        },
        9911: function (t, e, n) {
            "use strict";
            n.d(e, {
                Z: function () {
                    return C
                }
            });
            var i = n(7294),
                o = n(9163),
                l = n(6358),
                r = n(9664),
                a = n.n(r),
                c = n(7087),
                d = n(4640),
                u = n(2430),
                s = n(1229),
                p = n(4121),
                f = n(8347),
                m = n(131),
                g = i.createElement,
                v = o.ZP.video.withConfig({
                    displayName: "LazyVideo__StyledVideo",
                    componentId: "sc-12k5ev0-0"
                })(["display:block;width:100%;height:100%;object-fit:cover;"]),
                h = function (t) {
                    var e = t.src,
                        n = t.style,
                        o = t.className,
                        l = (0, f.Z)(t, ["src", "style", "className"]),
                        r = (0, i.useRef)(!1),
                        a = (0, m.YD)({
                            rootMargin: "100%",
                            triggerOnce: !0
                        }),
                        c = (0, p.Z)(a, 2),
                        d = c[0],
                        u = c[1],
                        h = (0, m.YD)(),
                        y = (0, p.Z)(h, 3),
                        b = y[0],
                        x = y[1],
                        _ = y[2];
                    return (0, i.useEffect)((function () {
                        var t = null === _ || void 0 === _ ? void 0 : _.target;
                        t && (u && !r.current && (t.load(), r.current = !0), x ? function (t) {
                            if (t) {
                                var e = t.play();
                                void 0 !== e && e.then((function () {})).catch((function () {}))
                            }
                        }(t) : function (t) {
                            var e = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
                            t && (t.pause(), e && (t.currentTime = 0))
                        }(t))
                    }), [x, u, _]), g("div", {
                        style: n,
                        className: o,
                        ref: d
                    }, g(v, (0, s.Z)({
                        playsInline: !0,
                        preload: "none",
                        ref: b
                    }, l), g("source", {
                        src: e
                    })))
                },
                y = i.createElement,
                b = o.ZP.div.withConfig({
                    displayName: "BigTextWithVideo__JumboTextWrap",
                    componentId: "sc-1pyvynd-0"
                })(["position:relative;text-align:center;font-size:0;"]),
                x = (0, o.ZP)(h).withConfig({
                    displayName: "BigTextWithVideo__MaskedVideo",
                    componentId: "sc-1pyvynd-1"
                })(["position:absolute;top:1px;left:1px;height:calc(100% - 2px);width:calc(100% - 2px);.ReactModal__Body--open &{opacity:0;}"]),
                _ = o.ZP.svg.withConfig({
                    displayName: "BigTextWithVideo__VideoMask",
                    componentId: "sc-1pyvynd-2"
                })(["position:relative;font-family:var(--secondary-font);text-transform:uppercase;font-size:", ";line-height:.93;.filler{visibility:hidden;}"], (0, c.hO)(400)),
                w = o.ZP.rect.withConfig({
                    displayName: "BigTextWithVideo__BGRect",
                    componentId: "sc-1pyvynd-3"
                })(["fill:var(--brand-white);"]),
                k = o.ZP.rect.withConfig({
                    displayName: "BigTextWithVideo__BlackBox",
                    componentId: "sc-1pyvynd-4"
                })(["fill:#000;"]),
                Z = (0, o.ZP)(u.Z).withConfig({
                    displayName: "BigTextWithVideo__StyledPlayButton",
                    componentId: "sc-1pyvynd-5"
                })(["position:absolute;top:50%;left:50%;transform:translate(-50%,-50%);"]),
                C = function (t) {
                    var e = t.topText,
                        n = t.bottomText,
                        o = t.video,
                        r = t.videoId,
                        c = (0, d.dd)().toggleModal,
                        u = (0, i.useRef)(),
                        s = (0, i.useRef)(),
                        p = (0, i.useRef)(),
                        f = (0, i.useCallback)((function (t) {
                            if (t) {
                                var e = t.querySelector(".filler"),
                                    n = t.querySelector(".the-text"),
                                    i = t.querySelector(".black-box"),
                                    o = t.querySelector(".play-button");
                                u.current = t, l.ZP.set([e, n, i], {
                                    autoAlpha: 1,
                                    transformOrigin: "50% 50%"
                                }), l.ZP.to(n, {
                                    x: 1
                                }), p.current = l.ZP.timeline({
                                    paused: !0,
                                    onComplete: function () {
                                        s.current.progress(0).pause()
                                    }
                                }).to(i, {
                                    opacity: 1,
                                    duration: 2
                                }).fromTo(i, {
                                    opacity: 1
                                }, {
                                    opacity: 0,
                                    duration: 1
                                }), s.current = l.ZP.timeline({
                                    paused: !0,
                                    onComplete: function () {
                                        c(r), p.current.play(0)
                                    }
                                }).fromTo(n, {
                                    scaleY: 1,
                                    scaleX: 1
                                }, {
                                    scaleY: .333,
                                    scaleX: 1.333,
                                    duration: .333
                                }, 0).fromTo(o, {
                                    opacity: 1,
                                    scale: 1
                                }, {
                                    opacity: 0,
                                    scale: .5
                                }, 0).fromTo([e, i], {
                                    scaleY: 0,
                                    scaleX: .5
                                }, {
                                    ease: "circ.inOut",
                                    scaleY: 1,
                                    scaleX: 1,
                                    duration: .5
                                }, 0).fromTo(i, {
                                    opacity: 0
                                }, {
                                    opacity: 1,
                                    duration: 1
                                }, "-=.333")
                            }
                        }), [c, r]);
                    return (0, i.useEffect)((function () {
                        return function () {
                            s.current && s.current.kill(), p.current && p.current.kill()
                        }
                    }), []), y(b, {
                        ref: f
                    }, y(x, {
                        muted: !0,
                        loop: !0,
                        playsInline: !0,
                        src: null === o || void 0 === o ? void 0 : o.url
                    }), y(_, {
                        viewBox: "0 0 1280 720"
                    }, y("g", {
                        fillRule: "evenodd"
                    }, y("mask", {
                        id: "text-mask-".concat(r)
                    }, y("rect", {
                        fill: "#fff",
                        x: "0",
                        y: "0",
                        width: "1280",
                        height: "720"
                    }), y("rect", {
                        className: "filler",
                        fill: "#000",
                        x: "0",
                        y: "0",
                        width: "1280",
                        height: "720"
                    }), y("g", {
                        className: "the-text"
                    }, y("text", {
                        fill: "#000",
                        x: "640",
                        y: "345",
                        textAnchor: "middle"
                    }, e), y("text", {
                        fill: "#000",
                        x: "640",
                        y: "335",
                        textAnchor: "middle",
                        dominantBaseline: "hanging"
                    }, n)))), y(w, {
                        mask: "url(#text-mask-".concat(r, ")"),
                        x: "0",
                        y: "0",
                        width: "1280",
                        height: "720"
                    }), y(k, {
                        className: "black-box",
                        x: "0",
                        y: "0",
                        width: "1280",
                        height: "720",
                        opacity: "0"
                    })), r ? y(Z, {
                        className: "play-button",
                        onClick: function () {
                            s.current.play(0), l.ZP.registerPlugin(a()), l.ZP.to(window, {
                                duration: .4,
                                scrollTo: {
                                    y: u.current,
                                    offsetY: (window.innerHeight - u.current.getBoundingClientRect().height) / 2,
                                    autoKill: !1
                                }
                            })
                        },
                        label: "Play video"
                    }) : null)
                }
        },
        6232: function (t, e, n) {
            "use strict";
            n.d(e, {
                v: function () {
                    return m
                }
            });
            var i = n(6265),
                o = n(7294),
                l = n(9163),
                r = n(5697),
                a = n.n(r),
                c = n(2751),
                d = n(4356),
                u = n(2334),
                s = o.createElement;

            function p(t, e) {
                var n = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var i = Object.getOwnPropertySymbols(t);
                    e && (i = i.filter((function (e) {
                        return Object.getOwnPropertyDescriptor(t, e).enumerable
                    }))), n.push.apply(n, i)
                }
                return n
            }

            function f(t) {
                for (var e = 1; e < arguments.length; e++) {
                    var n = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? p(Object(n), !0).forEach((function (e) {
                        (0, i.Z)(t, e, n[e])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(n)) : p(Object(n)).forEach((function (e) {
                        Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(n, e))
                    }))
                }
                return t
            }
            var m = function () {
                    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                        e = (0, o.useState)(0),
                        i = e[0],
                        l = e[1],
                        r = (0, o.useRef)(),
                        a = (0, o.useCallback)((function (e) {
                            if (null !== e) {
                                var i = new(n(2442))(e, f({
                                    prevNextButtons: !1,
                                    pageDots: !1,
                                    wrapAround: !0,
                                    dragThreshold: 5
                                }, t));
                                r.current = i, i.on("dragStart", (function () {
                                    document.ontouchmove = function (t) {
                                        t.preventDefault()
                                    }
                                })), i.on("dragEnd", (function () {
                                    document.ontouchmove = function () {
                                        return !0
                                    }
                                }))
                            }
                        }), [r, t]),
                        c = function (t) {
                            r && r.current && r.current.select(t, !0)
                        };
                    return (0, o.useEffect)((function () {
                        r && r.current && r.current.on("change", (function (t) {
                            l(t)
                        }))
                    }), [r]), (0, o.useEffect)((function () {
                        return function () {
                            r.current && r.current.destroy()
                        }
                    }), [r]), {
                        flickity: a,
                        flickityIndex: i,
                        setFlickityIndex: c
                    }
                },
                g = l.ZP.div.withConfig({
                    displayName: "Carousel",
                    componentId: "yahwoi-0"
                })([]);
            g.Slide = l.ZP.div.withConfig({
                displayName: "Carousel__Slide",
                componentId: "yahwoi-1"
            })(["width:100%;opacity:0;transition:opacity 300ms ease-in-out;&.is-selected{opacity:1;}"]);
            var v = l.ZP.li.withConfig({
                    displayName: "Carousel__SlideDot",
                    componentId: "yahwoi-2"
                })(["display:inline-block;&:not(:last-of-type){margin-right:10px;}"]),
                h = (0, l.ZP)(u.Z).withConfig({
                    displayName: "Carousel__SlideDotButton",
                    componentId: "yahwoi-3"
                })(["width:7px;height:7px;position:relative;display:inline-block;background-color:var(--", ');border-radius:50%;transition:opacity .33s;opacity:.5;&[aria-current="true"]{opacity:1;}&::before{content:"";display:block;position:absolute;top:-6px;right:-6px;bottom:-6px;left:-6px;}', ""], (function (t) {
                    return t.isDark ? "brand-white" : "gold"
                }), (0, c.M)("\n    opacity: 1;\n  ")),
                y = (0, l.ZP)(u.Z).withConfig({
                    displayName: "Carousel__SlideArrowButton",
                    componentId: "yahwoi-4"
                })(["", " ", ";transition:color 300ms ease-in-out;", ""], (function (t) {
                    return t.color && "\n    color: var(--".concat(t.color, ");\n  ")
                }), (function (t) {
                    return t.isLeft && "transform: scaleX(-1)"
                }), (0, c.M)("\n    color: var(--gold);\n  ")),
                b = l.ZP.svg.withConfig({
                    displayName: "Carousel__SlideArrow",
                    componentId: "yahwoi-5"
                })(["width:32px;height:8px;color:inherit;"]);
            g.Dots = function (t) {
                var e = t.slides,
                    n = void 0 === e ? [] : e,
                    i = t.currentIndex,
                    o = t.handleClick,
                    l = t.className,
                    r = t.isDark;
                return !n || n && n.length < 2 ? null : s(d.Z, {
                    className: l
                }, n.map((function (t, e) {
                    return s(v, {
                        key: e,
                        display: "inline-block"
                    }, s(h, {
                        "aria-label": "Go to slide ".concat(e + 1),
                        "aria-current": e === i,
                        isDark: r,
                        onClick: function () {
                            o(e)
                        }
                    }))
                })))
            }, g.Dots.propTypes = {
                slides: a().array,
                currentIndex: a().number,
                handleClick: a().func,
                className: a().string
            }, g.Button = function (t) {
                var e = t.slideCount,
                    n = t.currentIndex,
                    i = t.handleClick,
                    o = t.action,
                    l = t.isLeft,
                    r = t.color,
                    a = null,
                    c = "next" === o ? 1 : -1;
                return a = n + c >= e ? 0 : n + c < 0 ? e - 1 : n + c, s(y, {
                    "aria-label": "Go to ".concat(o, " slide"),
                    onClick: function () {
                        i(a)
                    },
                    action: o,
                    isLeft: l,
                    color: r
                }, s(b, {
                    viewBox: "0 0 32 8",
                    isLeft: l,
                    fill: "none",
                    xmlns: "http://www.w3.org/2000/svg"
                }, s("path", {
                    d: "M3.99 5H31.5V3H3.99V0L0 4l3.99 4V5z",
                    fill: "currentColor"
                })))
            }, e.Z = g
        },
        410: function (t, e, n) {
            "use strict";
            var i = n(7294),
                o = n(9163),
                l = n(2181),
                r = n(845),
                a = i.createElement,
                c = (0, o.ZP)(l.Z).withConfig({
                    displayName: "OffsetImageGrid__ImageGrid",
                    componentId: "sc-1m4jsr5-0"
                })(["grid-template-columns:", ";grid-gap:30px;"], (function (t) {
                    return t.smallImageLeft ? "1fr 2.5fr" : "2.5fr 1fr"
                })),
                d = (0, o.ZP)(r.Z).withConfig({
                    displayName: "OffsetImageGrid__SmallImage",
                    componentId: "sc-1m4jsr5-1"
                })(["order:", ";"], (function (t) {
                    return t.smallImageLeft ? "1" : "2"
                })),
                u = (0, o.ZP)(r.Z).withConfig({
                    displayName: "OffsetImageGrid__BigImage",
                    componentId: "sc-1m4jsr5-2"
                })(["order:", ";"], (function (t) {
                    return t.smallImageLeft ? "2" : "1"
                }));
            e.Z = function (t) {
                var e = t.smallImageLeft,
                    n = t.className,
                    i = t.smallImage,
                    o = t.bigImage;
                return a(c, {
                    smallImageLeft: e,
                    className: n
                }, null !== i && void 0 !== i && i.url ? a(d, {
                    width: 397,
                    height: 544,
                    src: "".concat(i.url, "&w=397&h=544&fit=crop&q=85&f=center"),
                    smallImageLeft: e,
                    alt: i.alt
                }) : null, null !== o && void 0 !== o && o.url ? a(u, {
                    width: 968,
                    height: 544,
                    src: "".concat(o.url, "&w=968&h=544&fit=crop&q=85&f=center"),
                    smallImageLeft: e,
                    alt: o.alt
                }) : null)
            }
        },
        6117: function (t, e, n) {
            "use strict";
            n.r(e), n.d(e, {
                __N_SSG: function () {
                    return lt
                },
                default: function () {
                    return rt
                }
            });
            var i = n(7294),
                o = n(4593),
                l = n(9163),
                r = n(2792),
                a = n(2751),
                c = n(8092),
                d = n(7706),
                u = n(9911),
                s = i.createElement,
                p = (0, l.ZP)(c.Z).withConfig({
                    displayName: "Video__StyledSection",
                    componentId: "bo9b66-0"
                })(["padding:320px 0 var(--spacing);", " ", ""], r.Z.below(a.bp.desktop, "\n    padding-top: 240px;\n  "), r.Z.below(a.bp.laptopSm, "\n    padding-top: 160px;\n  ")),
                f = function (t) {
                    var e = t.topText,
                        n = t.bottomText,
                        i = t.video,
                        o = t.videoId;
                    return s(p, {
                        bgColor: "brand-white"
                    }, s(d.Z, null, s(u.Z, {
                        topText: e,
                        bottomText: n,
                        video: i,
                        videoId: o
                    })))
                },
                m = n(6232),
                g = n(2181),
                v = n(1757),
                h = n(410),
                y = n(4356),
                b = n(9544),
                x = i.createElement,
                _ = (0, l.ZP)(g.Z).withConfig({
                })([]),
                w = l.ZP.div.withConfig({
                })([]),
                k = l.ZP.nav.withConfig({
                    displayName: "CarouselBlock__CarouselNavigation",
                    componentId: "sc-71s561-2"
                })([]),
                Z = function (t) {
                    var e = t.title,
                        n = t.subTitle,
                        i = t.description,
                        o = t.slides,
                        l = void 0 === o ? [] : o,
                        r = (0, m.v)(),
                        a = r.flickity,
                        u = r.setFlickityIndex,
                        s = r.flickityIndex;
                    return x(c.Z, {
                        hasPadding: !0,
                        bgColor: "brand-black"
                    }, x(d.Z, null, x(_, null, x("div", null, e ? x(v.zi, null, e) : null), x("div", null, n ? x("h2", null, n) : null, i ? x(b.Z, {
                        content: i
                    }) : null)), x(w, null, x(m.Z, {
                        ref: a
                    }, l.map((function (t, e) {
                        return x(m.Z.Slide, {
                            key: e
                        }, x(h.Z, {
                            smallImageLeft: !0,
                            smallImage: null === t || void 0 === t ? void 0 : t.left_image,
                            bigImage: null === t || void 0 === t ? void 0 : t.right_image
                        }))
                    })))), x(k, {
                        "aria-label": "About Us Carousel"
                    }, x(y.Z, null, x("li", null, x(m.Z.Button, {
                        slideCount: l.length,
                        currentIndex: s,
                        handleClick: u,
                        action: "prev",
                        color: "brand-white"
                    })), x("li", null, x(m.Z.Button, {
                        slideCount: l.length,
                        currentIndex: s,
                        handleClick: u,
                        isLeft: !0,
                        action: "next",
                        color: "brand-white"
                    }))))))
                },
                C = n(845),
                I = i.createElement,
                P = (0, l.ZP)(g.Z).withConfig({
                    displayName: "TwoColumn__StyledGrid",
                    componentId: "sc-1p2444d-0"
                })(["grid-gap:0;", ""], r.Z.above(a.bp.tablet, "\n    grid-template-columns: repeat(2, 1fr);\n  ")),
                T = l.ZP.div.withConfig({
                    displayName: "TwoColumn__ImageColumn",
                    componentId: "sc-1p2444d-1"
                })(["position:relative;"]),
                N = l.ZP.div.withConfig({
                    displayName: "TwoColumn__ContentColumn",
                    componentId: "sc-1p2444d-2"
                })(["padding:var(--spacing) 146px;", " ", " ", ""], r.Z.below(a.bp.desktop, "\n    padding: var(--spacing) 110px;\n  "), r.Z.below(a.bp.laptop, "\n    padding: var(--spacing) 60px;\n  "), r.Z.below(a.bp.tablet, "\n    padding: var(--spacing) var(--container-gutter);\n  ")),
                S = (0, l.ZP)(C.Z).withConfig({
                    displayName: "TwoColumn__AbsoluteImage",
                    componentId: "sc-1p2444d-3"
                })(["", ""], r.Z.above(a.bp.tablet, "\n    position: absolute;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n\n    img {\n      position: absolute;\n      top: 0;\n      left: 0;\n      width: 100%;\n      height: 100%;\n      object-fit: cover;\n    }\n  ")),
                O = function (t) {
                    var e = t.title,
                        n = t.description,
                        i = t.image;
                    return I(c.Z, {
                        bgColor: "brand-white"
                    }, I(P, null, I(T, null, i ? I(S, {
                        src: "".concat(null === i || void 0 === i ? void 0 : i.url, "&w=900&h=900&fit=crop&q=85&f=center"),
                        alt: null === i || void 0 === i ? void 0 : i.alt,
                        width: 900,
                        height: 900
                    }) : null), I(N, null, e ? I("h2", null, e) : null, n ? I(b.Z, {
                        content: n
                    }) : null)))
                },
                B = n(6358),
                E = n(6546),
                j = n.n(E),
                D = n(4477),
                X = n(9778),
                L = n(8362),
                Y = i.createElement,
                G = (0, l.ZP)(c.Z).withConfig({
                    displayName: "TeamGrid__StyledSection",
                    componentId: "sc-2udcyt-0"
                })(["h2{margin-bottom:80px;text-align:center;}"]),
                A = (0, l.ZP)(g.Z).withConfig({
                    displayName: "TeamGrid__StyledGrid",
                    componentId: "sc-2udcyt-1"
                })(["grid-template-columns:repeat(4,1fr);grid-gap:48px 20px;max-width:1395px;margin:auto;li{visibility:hidden;}span,small{display:block;}", " ", " ", ""], r.Z.below(a.bp.desktopSm, "\n    grid-template-columns: repeat(3, 1fr);\n  "), r.Z.below(a.bp.portrait, "\n    grid-template-columns: repeat(2, 1fr);\n  "), r.Z.below(a.bp.mobile, "\n    grid-template-columns: 1fr;\n  ")),
                V = l.ZP.span.withConfig({
                    displayName: "TeamGrid__Name",
                    componentId: "sc-2udcyt-2"
                })(["margin-top:25px;"]),
                W = l.ZP.div.withConfig({
                    displayName: "TeamGrid__Wrap",
                    componentId: "sc-2udcyt-3"
                })(['position:relative;&::before{content:"";width:100%;padding-bottom:', ";}> .content{position:absolute;top:0;left:0;bottom:0;display:flex;align-items:center;justify-content:center;flex-direction:column;}"], (0, D.Z)(370, 330)),
                q = (0, l.ZP)(W).withConfig({
                    displayName: "TeamGrid__GrowingWrap",
                    componentId: "sc-2udcyt-4"
                })(["display:flex;align-items:center;justify-content:center;color:var(--gold);background-color:var(--brand-white);text-align:center;transition:color 300ms ease-in-out,background-color 300ms ease-in-out;h3{color:var(--brand-black);}svg{max-width:11px;margin:22px auto 16px;color:inherit;}", ""], (0, a.M)("\n    color: var(--brand-white);\n    background-color: var(--gold);\n  ")),
                M = function (t) {
                    var e = t.title,
                        n = t.team,
                        o = void 0 === n ? [] : n,
                        l = t.linkTopText,
                        r = t.linkBottomText,
                        a = t.link,
                        c = (0, i.useCallback)((function (t) {
                            if (t) {
                                B.ZP.registerPlugin(j());
                                var e = t.children;
                                e.length > 0 && j().batch(e, {
                                    onEnter: function (t) {
                                        return B.ZP.fromTo(t, {
                                            autoAlpha: 0,
                                            yPercent: 20
                                        }, {
                                            duration: 1.5,
                                            autoAlpha: 1,
                                            yPercent: 0,
                                            stagger: {
                                                each: .25
                                            },
                                            overwrite: !0,
                                            ease: "power2"
                                        })
                                    },
                                    id: "team-grid-trigger",
                                    start: "top 80%",
                                    end: "+=0",
                                    once: !0
                                })
                            }
                        }), []);
                    return (0, i.useEffect)((function () {
                        return function () {
                            j().getById("team-grid-trigger") && j().getById("team-grid-trigger").kill()
                        }
                    }), []), Y(G, {
                        hasPadding: !0
                    }, Y(d.Z, null, e ? Y("h2", null, e) : null, o.length > 0 ? Y(y.Z, {
                        ref: c,
                        as: A
                    }, o.map((function (t, e) {
                        var n, i;
                        return Y("li", {
                            key: e
                        }, Y(C.Z, {
                            width: 330,
                            height: 370,
                            src: "".concat(null === t || void 0 === t || null === (n = t.image) || void 0 === n ? void 0 : n.url, "&w=330&h=370&fit=crop&q=85&f=center"),
                            alt: null === t || void 0 === t || null === (i = t.image) || void 0 === i ? void 0 : i.alt
                        }), Y(V, null, Y("strong", null, null === t || void 0 === t ? void 0 : t.name)), Y("small", null, null === t || void 0 === t ? void 0 : t.job_title))
                    })), Y("li", null, Y(q, {
                        as: L.Z,
                        href: null === a || void 0 === a ? void 0 : a.url
                    }, Y("div", {
                        className: "content"
                    }, l ? Y(v.FS, {
                        as: "h3"
                    }, l) : null, Y(X.Z, null), r ? Y(v.lC, null, r) : null)))) : null))
                },
                R = n(7087),
                K = n(1432),
                z = n(9520),
                F = i.createElement,
                U = (0, l.ZP)(c.Z).withConfig({
                    displayName: "TheDuke__StyledSection",
                    componentId: "buks1g-0"
                })(["text-align:center;img{display:block;margin:-50px auto auto;", "}span,small{display:block;}"], r.Z.below(a.bp.laptopSm, "\n      margin-top: -25px;\n    ")),
                H = l.ZP.div.withConfig({
                    displayName: "TheDuke__BigTextWrap",
                    componentId: "buks1g-1"
                })(["max-width:1118px;margin:auto;", " ", ""], r.Z.below(a.bp.desktop, "\n    max-width: 840px;\n  "), r.Z.below(a.bp.portrait, "\n    max-width: 500px;\n  ")),
                J = l.ZP.div.withConfig({
                    displayName: "TheDuke__Description",
                    componentId: "buks1g-2"
                })(["max-width:520px;margin:60px auto auto;"]),
                Q = l.ZP.figure.withConfig({
                    displayName: "TheDuke__ImageWrap",
                    componentId: "buks1g-3"
                })(["position:relative;text-align:left;"]),
                $ = l.ZP.figcaption.withConfig({
                    displayName: "TheDuke__ImageCaption",
                    componentId: "buks1g-4"
                })(["position:absolute;left:50%;bottom:10px;transform:translateX(-50%);span{line-height:1;}small{font-size:", ";}", ""], (0, R.hO)(12), r.Z.below(a.bp.mobile, "\n    left: calc(50% + 25px);\n    bottom: -25px;\n  ")),
                tt = function (t) {
                    var e = t.title,
                        n = t.image,
                        i = t.caption,
                        o = t.subCaption,
                        l = t.description,
                        r = t.linkUrl,
                        a = t.linkText;
                    return F(U, {
                        hasPadding: !0,
                        bgColor: "brand-white"
                    }, F(d.Z, null, e ? F(H, null, F(v.D0, null, e)) : null, F(Q, null, n ? F(C.Z, {
                        src: "".concat(null === n || void 0 === n ? void 0 : n.url, "&w=553&h=372&fit=crop&q=85&f=center"),
                        alt: null === n || void 0 === n ? void 0 : n.alt
                    }) : null, F($, null, i ? F("span", null, F("strong", null, i)) : null, o ? F("small", null, o) : null)), l ? F(J, null, F(b.Z, {
                        content: l
                    })) : null, r ? F(z.Z, {
                        hasmargin: !0,
                        isdark: !0,
                        large: !0,
                        bgcolor: "gold",
                        href: (0, K.kG)(r)
                    }, a) : null))
                },
                et = n(1407),
                nt = n(4640),
                it = n(8130),
                ot = i.createElement,
                lt = !0,
                rt = function (t) {
                    var e, n, l = t.page,
                        r = (0, nt.dd)().modalOpen;
                    if (!l) return null;
                    var a = l.data;
                    return ot(i.Fragment, null, ot(o.q, {
                        title: null === a || void 0 === a ? void 0 : a.page_title,
                        meta: [{
                            name: "description",
                            content: null === a || void 0 === a ? void 0 : a.page_description
                        }, {
                            property: "og:title",
                            content: null === a || void 0 === a ? void 0 : a.page_title
                        }, {
                            property: "og:description",
                            content: null === a || void 0 === a ? void 0 : a.page_description
                        }, {
                            property: "og:image",
                            content: "".concat(null === a || void 0 === a || null === (e = a.page_social_image) || void 0 === e ? void 0 : e.url, "&w=1200&h=630&fit=crop&q=85&f=center")
                        }, {
                            name: "twitter:image",
                            content: "".concat(null === a || void 0 === a || null === (n = a.page_social_image) || void 0 === n ? void 0 : n.url, "&w=1200&h=630&fit=crop&q=85&f=center")
                        }, {
                            name: "twitter:title",
                            content: null === a || void 0 === a ? void 0 : a.page_title
                        }, {
                            name: "twitter:description",
                            content: null === a || void 0 === a ? void 0 : a.page_description
                        }]
                    }), ot("main", null, ot(f, {
                        topText: null === a || void 0 === a ? void 0 : a.hero_top_text,
                        bottomText: null === a || void 0 === a ? void 0 : a.hero_bottom_text,
                        video: null === a || void 0 === a ? void 0 : a.hero_video,
                        videoId: null === a || void 0 === a ? void 0 : a.hero_video_id
                    }), ot(Z, {
                        title: null === a || void 0 === a ? void 0 : a.intro_title,
                        subTitle: null === a || void 0 === a ? void 0 : a.intro_sub_title,
                        description: null === a || void 0 === a ? void 0 : a.intro_description,
                        slides: null === a || void 0 === a ? void 0 : a.intro_carousel
                    }), ot(O, {
                        title: null === a || void 0 === a ? void 0 : a.two_column_title,
                        description: null === a || void 0 === a ? void 0 : a.two_column_description,
                        image: null === a || void 0 === a ? void 0 : a.two_column_image
                    }), ot(M, {
                        title: null === a || void 0 === a ? void 0 : a.team_grid_title,
                        team: null === a || void 0 === a ? void 0 : a.team_member,
                        linkTopText: null === a || void 0 === a ? void 0 : a.team_grid_link_top_text,
                        linkBottomText: null === a || void 0 === a ? void 0 : a.team_grid_link_bottom_text,
                        link: null === a || void 0 === a ? void 0 : a.team_grid_link
                    }), ot(tt, {
                        title: null === a || void 0 === a ? void 0 : a.pre_footer_title,
                        image: null === a || void 0 === a ? void 0 : a.pre_footer_image,
                        caption: null === a || void 0 === a ? void 0 : a.pre_footer_caption,
                        subCaption: null === a || void 0 === a ? void 0 : a.pre_footer_sub_caption,
                        description: null === a || void 0 === a ? void 0 : a.pre_footer_description,
                        linkUrl: null === a || void 0 === a ? void 0 : a.pre_footer_link,
                        linkText: null === a || void 0 === a ? void 0 : a.pre_footer_link_text
                    })), ot(et.Z, null, ot(it.Z, {
                        youTubeID: r
                    })))
                }
        },
        2088: function (t, e, n) {
            (window.__NEXT_P = window.__NEXT_P || []).push(["/about", function () {
                return n(6117)
            }])
        }
    },
    function (t) {
        t.O(0, [774, 351, 663, 46, 258, 833, 800, 442, 817], (function () {
            return e = 2088, t(t.s = e);
            var e
        }));
        var e = t.O();
        _N_E = e
    }
]);